import matplotlib.pyplot as plt
# import pydicom
# from pydicom.data import get_testdata_files
import os
import numpy as np
import cv2

# Specify the .dcm folder path
# folder_path = "C:\\Users\\lappa\\Desktop\\A_patient\\"
# folder_path = "C:\\Users\\lappa\\Desktop\\A_JPG\\"
folder_path = "C:\\Users\\lappa\\Desktop\\DICOMvideo - Copy\\"

len_im = 160
ROI_s = 40
ROI_e = 120

mask_arr = np.zeros(shape=(len_im, len_im), dtype=np.uint8)
# mask_arr[ROI_s:ROI_e, ROI_s:ROI_e] = np.ones(shape=(ROI_e - ROI_s, ROI_e - ROI_s))
ROI_arr = np.zeros(shape=(len_im, len_im), dtype=np.uint8)
ROI_arr[ROI_s:ROI_e, ROI_s:ROI_e] = np.ones(shape=(ROI_e - ROI_s, ROI_e - ROI_s))

# filename = get_testdata_files(os.path.join(folder_path + "I82.dcm"))
for i in range(108, 880):

    # ds = pydicom.dcmread(os.path.join(folder_path + "I" + str(i) + ".dcm"), force=True)

    # # ds.file_meta.TransferSyntaxUID = pydicom.uid.ImplicitVRLittleEndian # or whatever is the correct transfer syntax for the file
    img = cv2.imread(folder_path + 'I' + str(i) + '.png') # , cv2.IMREAD_UNCHANGED)
    # # a = ds.pixel_array

    # plt.imshow(ds.pixel_array, cmap=plt.cm.bone)

    ROI = img[40:120, 40:120]

    result = np.where((ROI >= 200))
    result2 = np.where((img >= 215))

    mask_arr[result2[0], result2[1]] = 1  # np.ones(shape=(len(result[0]), len(result[0])))

    for i in range(0, 40):
        mask_arr[i, :] = 0

    for i in range(120, len(mask_arr)):
        mask_arr[i, :] = 0

    for j in range(0, 40):
        mask_arr[:, j] = 0

    for j in range(120, len(mask_arr)):
        mask_arr[:, j] = 0

    contours0, hierarchy = cv2.findContours(mask_arr.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(img, contours0, -1, (0, 255, 0))

    dummy = np.zeros(shape=(160, 160))
    dummy[40:120, 40:120] = 1

    plt.figure()
    plt.subplot(121)
    plt.imshow(img)
    plt.subplot(122)
    plt.imshow(mask_arr, cmap=plt.cm.bone)
    plt.show()

    # plt.show(block = False)
    # plt.pause(0.5)
    # plt.close()