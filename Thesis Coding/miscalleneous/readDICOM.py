import os

PathDicom = "C:\\Users\\lappa\\Desktop\\Twente\\Master Thesis\\Practical\\Dataset\\MunsterAll\\MRI\\B\\DICOM\\"
lstFilesDCM = []  # create an empty list
final_list = []
name_list = []

for i in range(82, 881):
    data_name = 'I' + str(i)
    name_list.append(data_name)

for dirName, subdirList, fileList in os.walk(PathDicom):
    for filename in fileList:
        lstFilesDCM.append(os.path.join(dirName, filename))

for cnt in range(0, len(lstFilesDCM)):
    for j in range(0, len(name_list)):
        if name_list[j] == lstFilesDCM[cnt].split(PathDicom)[1]:
            # print("found in %s" % lstFilesDCM[cnt])
            final_list.append(lstFilesDCM[cnt])

save_path = 'C:\\Users\\lappa\\Desktop\\B_patient_DICOM\\'
for w in range(0, len(final_list)):

    name_of_file = final_list[w].split('DICOM\\')[1]
    completeName = os.path.join(save_path, name_of_file + '.dcm') # '.dcm'
    file1 = open(completeName, "w")
    toFile = final_list[w]
    file1.write(toFile)
    file1.close()