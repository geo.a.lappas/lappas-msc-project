# This script finds the autocorrelation function to check the correlation between a signal and a lagged instance of it
# INPUT: directory, filename
# OUTPUT: ACF plot

# from pandas import read_csv
import pandas as pd
from statsmodels.graphics.tsaplots import plot_acf
import matplotlib.pyplot as plt
import numpy as np


def ACF_plot(directory_n, file_n):
    directory_new = directory_n
    s = pd.read_csv(directory_new + file_n, usecols=[0])
    series = s['displacement']

    fig = plt.figure()
    # plt.plot(series)
    # plot_acf(series, unbiased=True, fft=True, lags=np.arange(len(series)/2))
    plt.acorr(series, maxlags=int(series.size/2) - 1)
    plt.show()

directory_n = "C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\"
file_n = "Resampled A_internal_motion.csv"

ACF_plot(directory_n, file_n)