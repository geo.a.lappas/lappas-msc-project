import cv2

# folder_path = "C:\\Users\\lappa\\Desktop\\PNG_160\\PNG_E_C\\"
folder_path = "C:\\Users\\lappa\\Desktop\\PNG_160\\PNG_E_R\\"
# save_path = "C:\\Users\\lappa\\Desktop\\PNG_480\\E_Cartesian\\"
save_path = "C:\\Users\\lappa\\Desktop\\PNG_480\\E_Radial\\"

for i in range(1765, 2543):
    img = cv2.imread(folder_path + 'I' + str(i) + '.png', cv2.IMREAD_UNCHANGED)

    print('Original Dimensions : ', img.shape)

    scale_percent = 300  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

    print('Resized Dimensions : ', resized.shape)

    cv2.imwrite(save_path + 'I' + str(i) + '.png', resized)

    # cv2.imshow("Resized image", resized)
    cv2.waitKey(0)
    cv2.destroyAllWindows()