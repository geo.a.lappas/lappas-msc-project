import numpy as np
import pandas as pd
import statsmodels.api as sm
from scipy.optimize import minimize
from scipy.ndimage.interpolation import shift
from scipy.signal import savgol_filter as savgol
from scipy.signal import butter, correlate
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from sklearn.preprocessing import PolynomialFeatures
# import perceptron

"""
MIT License

Copyright (c) 2018 Thomas Countz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

class Perceptron(object):

    def __init__(self, no_of_inputs, threshold=1000, learning_rate=0.01):
        self.threshold = threshold
        self.learning_rate = learning_rate
        self.weights = np.zeros(no_of_inputs + 1)

    def predict(self, inputs):
        summation = np.dot(inputs, self.weights[1:]) + self.weights[0]
        if summation > 0:
            activation = 1
        else:
            activation = 0
        return activation

    def train(self, training_inputs, labels):
        for _ in range(self.threshold):
            for inputs, label in zip(training_inputs, labels):
                prediction = self.predict(inputs)
                self.weights[1:] += self.learning_rate * (label - prediction) * inputs
                self.weights[0] += self.learning_rate * (label - prediction)

# Modified code taken from Pearson's work: https://github.com/pearsonkyle/Signal-Alignment
# Corresponding citation:
# @article{Pearson2019,
#   author={Kyle A. Pearson and Caitlin A. Griffith and Robert T. Zellem and Tommi T. Koskinen and Gael M. Roudier},
#   title={Ground-based Spectroscopy of the Exoplanet XO-2b Using a Systematic Wavelength Calibration},
#   journal={The Astronomical Journal},
#   volume={157},
#   number={1},
#   pages={21},
#   url={http://stacks.iop.org/1538-3881/157/i=1/a=21},
#   year={2019}
# }


def equalize_array_size(array1,array2,cut_m=1):
    '''
    reduce the size of one sample to make them equal size. 
    The sides of the biggest signal are truncated

    Args:
        array1 (1d array/list): signal for example the reference
        array2 (1d array/list): signal for example the target

    Returns:
        array1 (1d array/list): middle of the signal if truncated
        array2 (1d array/list): middle of the initial signal if there is a size difference between the array 1 and 2
        dif_length (int): size diffence between the two original arrays 
    '''

    len1, len2 = len(array1), len(array2)
    dif_length = len1-len2
    if dif_length<0:
        if cut_m == 1: array2 = array2[int(np.floor(-dif_length/2)):len2-int(np.ceil(-dif_length/2))]
        else: array2 = array2[0:len2 - int(np.ceil(-dif_length))]
    elif dif_length>0:
        if cut_m == 1: array1 = array1[int(np.floor(dif_length/2)):len1-int(np.ceil(dif_length/2))]
        else: array1 = array1[0:len1 - int(np.ceil(dif_length))]

    return array1, array2, dif_length

def chisqr_align(reference, target, roi=None, order=1, init=0.1, bound=1):
    '''
    Align a target signal to a reference signal within a region of interest (ROI)
    by minimizing the chi-squared between the two signals. Depending on the shape
    of your signals providing a highly constrained prior is necessary when using a
    gradient based optimization technique in order to avoid local solutions.

    Args:
        reference (1d array/list): signal that won't be shifted
        target (1d array/list): signal to be shifted to reference
        roi (tuple): region of interest to compute chi-squared
        order (int): order of spline interpolation for shifting target signal
        init (int):  initial guess to offset between the two signals
        bound (int): symmetric bounds for constraining the shift search around initial guess

    Returns:
        shift (float): offset between target and reference signal
    '''

    reference, target, dif_length = equalize_array_size(reference, target)

    if roi==None: roi = [0,len(reference)-1]
    # convert to int to avoid indexing issues
    ROI = slice(int(roi[0]), int(roi[1]), 1)

    # normalize ref within ROI
    reference = reference/np.mean(reference[ROI])

    # define objective function: returns the array to be minimized
    def fcn2min(x):
        shifted = shift(target, x, order=order)
        shifted = shifted/np.mean(shifted[ROI])
        return np.sum(((reference - shifted)**2)[ROI])

    # set up bounds for pos/neg shifts
    minb = min( [(init-bound),(init+bound)] )
    maxb = max( [(init-bound),(init+bound)] )

    # minimize chi-squared between the two signals 
    result = minimize(fcn2min, init, method='L-BFGS-B', bounds=[(minb, maxb)])

    return result.x[0] + int(np.floor(dif_length/2))


if __name__ == "__main__":
    from scipy import signal
    import matplotlib.pyplot as plt

    a = pd.read_csv('C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\Resampled D_internal_motion.csv', usecols=[0])
    df_a = a['displacement']
    d_a = df_a.to_numpy()

    b = pd.read_csv('C:\\Users\\lappa\\Desktop\\ThesisData\\SurrogateSignal\\Dcartesian.csv', usecols=[0])
    df_b = b['depth2667']
    d_b = df_b.to_numpy()

    ref_signal = d_b # US signal - pulse receiver magnitude
    shift_signal = d_a # MRI signal - displacement (mm) in SI

    # smoothing process
    SG_ref_7 = savgol(ref_signal, 51, 7)
    SG_shift = savgol(shift_signal, 21, 4)

    # scaling for the internal motion data
    # SG_shift_scaled = 1 / 1550 * SG_shift + 4.86# 85500  # calculated manually

    # # Scaling based on Reference signal
    # mRef = np.mean(SG_ref_7)
    # stdRef = np.std(SG_ref_7)
    # mSig = np.mean(SG_shift)
    # stdSig = np.std(SG_shift)
    # SG_shift_scaled = ((SG_shift - mSig) / stdSig) * stdRef + mRef

    # Scaling based on Reference signal - internal motion
    mRef = np.mean(SG_ref_7)
    stdRef = np.std(SG_ref_7)
    mSig = np.mean(SG_shift)
    stdSig = np.std(SG_shift)
    SG_ref_7 = ((SG_ref_7 - mRef) / stdRef) * stdSig + mSig
    SG_shift_scaled = SG_shift

    # create same size for internal motion and surrogate signals - cutting difference at the end of signal
    SG_ref_7_1, SG_shift_scaled_1, dif_length = equalize_array_size(SG_ref_7, SG_shift_scaled, 2)

    # normalization of the signals
    SG_ref_7_norm = (SG_ref_7_1 - np.mean(SG_ref_7_1)) / (np.std(SG_ref_7_1))  # * len(SG_ref_7_1))
    SG_ref_7_norm_len = (SG_ref_7_1 - np.mean(SG_ref_7_1)) / (np.std(SG_ref_7_1)) * len(SG_ref_7_1)
    SG_shift_scaled_norm = (SG_shift_scaled_1 - np.mean(SG_shift_scaled_1)) / (np.std(SG_shift_scaled_1))


    # 1. chi-squared alignment
    s = chisqr_align(SG_ref_7_1, SG_shift_scaled_1, order=5, init=0.00184, bound=500) # 0.00503

    # 2. using time lag calculation from cross-correlation function
    lag = np.argmax(correlate(SG_ref_7_1, SG_shift_scaled_1))  # to find the difference lag between the two signals
    c_sig = np.roll(SG_shift_scaled_1, shift=int(np.ceil(lag)))  # shift the internal motion to fit the surrogate data

    # 3. cross-correlation and normalized cross-correlation
    x_corr = np.correlate(SG_ref_7_1, SG_shift_scaled_1, mode='same')
    normalized_corr = np.correlate(SG_ref_7_norm_len, SG_shift_scaled_norm, mode='same')

    # Linear Regression - Scikit learn

    X_train, X_test, y_train, y_test = train_test_split(SG_shift_scaled_1, SG_ref_7_1, test_size=0.2, random_state=0)

    X_train = np.expand_dims(X_train, axis=1)
    X_test = np.expand_dims(X_test, axis=1)
    y_train = np.expand_dims(y_train, axis=1)
    y_test = np.expand_dims(y_test, axis=1)

    diff_a = np.diff(np.sign(np.diff(SG_shift_scaled_1))).nonzero()[0] + 1  # local min & max - detect non zero points
    diff_b = (np.diff(np.sign(np.diff(SG_shift_scaled_1))) > 0).nonzero()[0] + 1  # local min - inferior
    diff_c = (np.diff(np.sign(np.diff(SG_shift_scaled_1))) < 0).nonzero()[0] + 1  # local max - superior

    superior_mean = (np.mean(SG_shift_scaled_1[diff_c]))
    inferior_mean = (np.mean(SG_shift_scaled_1[diff_b]))
    superior_std = (np.std(SG_shift_scaled_1[diff_c]))
    inferior_std = (np.std(SG_shift_scaled_1[diff_b]))
    superior_var = (np.var(SG_shift_scaled_1[diff_c]))
    inferior_var = (np.var(SG_shift_scaled_1[diff_b]))

    print("Subject A-superior:", format(superior_mean, '.2f'), "\u00B1", format(superior_std, '.2f'))
    print("Subject A-inferior:", format(inferior_mean, '.2f'), "\u00B1", format(inferior_std, '.2f'))

    # regressor = LinearRegression()
    # regressor.fit(X_train, y_train)  # training the algorithm
    #
    # y_pred = regressor.predict(X_test)

    # print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
    # print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
    # print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))


    # Polynomial fit - Quadratic

    def create_polynomial_regression_model(degree):
        "Creates a polynomial regression model for the given degree"

        X_train, X_test, y_train, y_test = train_test_split(SG_shift_scaled_1, SG_ref_7_1, test_size=0.2,
                                                            random_state=0)

        X_train = np.expand_dims(X_train, axis=1)
        X_test = np.expand_dims(X_test, axis=1)
        y_train = np.expand_dims(y_train, axis=1)
        y_test = np.expand_dims(y_test, axis=1)

        poly_features = PolynomialFeatures(degree=degree)

        # transforms the existing features to higher degree features.
        X_train_poly = poly_features.fit_transform(X_train)

        # fit the transformed features to Linear Regression
        poly_model = LinearRegression()
        poly_model.fit(X_train_poly, y_train)

        # predicting on training data-set
        y_train_predicted = poly_model.predict(X_train_poly)

        # predicting on test data-set
        y_test_predict = poly_model.predict(poly_features.fit_transform(X_test))

        # evaluating the model on training dataset
        rmse_train = np.sqrt(metrics.mean_squared_error(y_train, y_train_predicted))
        r2_train = metrics.r2_score(y_train, y_train_predicted)

        # evaluating the model on test dataset
        rmse_test = np.sqrt(metrics.mean_squared_error(y_test, y_test_predict))
        r2_test = metrics.r2_score(y_test, y_test_predict)

        print("The model performance for the training set")
        print("-------------------------------------------")
        print("RMSE of training set is {}".format(rmse_train))
        print("R2 score of training set is {}".format(r2_train))

        print("\n")

        print("The model performance for the test set")
        print("-------------------------------------------")
        print("RMSE of test set is {}".format(rmse_test))
        print("R2 score of test set is {}".format(r2_test))


    # create_polynomial_regression_model(2) # 2nd order polynomial

    # Neural Networks

    # training_inputs = X_train
    # labels = y_train
    #
    # perceptron = Perceptron(1)
    # perceptron.train(training_inputs, labels)
    #
    # inputs = y_test
    # y_pred = perceptron.predict(inputs)
    #
    # RMSE_perceptron = np.sqrt(metrics.mean_squared_error(y_test, y_pred))
    #
    # print(RMSE_perceptron)

    # plt.figure()

    # plt.subplot(231)  # plot normalized data
    # plt.title('Surrogate and internal motion normalized data representation')  # have same length
    # plt.plot(SG_ref_7_norm, label='Normalized surrogate data', c='C0')  # SG_ref_7_norm
    # plt.plot(SG_shift_scaled_norm, label='Normalized resampled internal data', c='C1') # SG_shift_scaled_norm
    # plt.legend(loc='best')
    # plt.show()

    # SG_ref_7_12, SG_shift_12, dif_length2 = equalize_array_size(SG_ref_7, SG_shift, 2)

    # plt.title('Scatterplot of surrogate and internal motion data')
    # plt.scatter(SG_shift_12[1000:1200], SG_ref_7_12[1000:1200]) # label='surrogate')
    # # plt.scatter(SG_shift_12[0:6000], SG_ref_7_12[0:6000]) #, label='internal motion')
    # plt.ylabel('Surrogate - pulse magnitude (mm)')
    # plt.xlabel('Internal SI-motion displacement (mm)')
    # # plt.legend(loc='best')
    # plt.show()

    # plot data
    # plt.title('Surrogate and internal motion data representation')  # have same length
    # plt.plot(SG_ref_7_1, label='Surrogate data', c='C0')  # SG_ref_7_norm
    # plt.plot(SG_shift_scaled_1, label='Resampled internal data', c='C1') # SG_shift_scaled_norm
    # plt.legend(loc='best')


    # # Histograms of normalized signals
    # plt.figure()
    # plt.subplot(121)
    # plt.hist(SG_shift_scaled_norm, 255, facecolor='red', ec="black", label="Normalized internal motion")
    # plt.title('Histogram of scaled and normalized internal motion data')  # have same length
    # plt.ylabel('Frequency')
    # plt.xlabel('SI-displacement in mm')
    # plt.legend(loc='best')
    # plt.subplot(122)
    # plt.hist(SG_ref_7_norm, 255, facecolor='red', ec="black", label="Normalized surrogate data")
    # plt.title('Histogram of normalized surrogate data')  # have same length
    # plt.ylabel('Frequency')
    # plt.xlabel('Magnitude in mm')
    # plt.legend(loc='best')
    # plt.show()

    # plt.subplot(232)  # normalized cross-correlation plot
    # plt.title('Normalized Cross-correlation of signals')
    # plt.plot(normalized_corr, label=' Normalized Cross-correlation function')
    # plt.plot(x_corr, label='Cross-correlation function')
    # plt.legend(loc='best')

    # plt.subplot(233)  # for checking cutting process of signals
    # plt.title('Cutting process of the signals')
    # plt.plot(SG_shift_scaled, label='Internal motion data', c='C0')
    # plt.plot(SG_shift_scaled_1, ls='--', label='Cutted Internal motion data', c='C1')
    # plt.plot(SG_ref_7, label='Surrogate data', c='C2')
    # plt.plot(SG_ref_7_1, ls='--', label='Cutted surrogate motion data', c='C3')
    # plt.legend(loc='best')

    # plt.subplot(234)  # 1. alignment checking  - have same length
    # plt.subplot(121)
    # plt.title('Shifting process of chi-squared minimization')
    # plt.plot(shift(SG_shift_scaled_1, s, mode='nearest'), ls='--', label='aligned data', c='C0')
    # plt.plot(SG_shift_scaled_1, label='Resampled smoothed internal data', c='C1')
    # plt.plot(SG_ref_7_1, label='Smoothed surrogate data', c='C2')
    # plt.legend(loc='best')
    #
    # # plt.subplot(235)  # 2. alignment checking using time lag - have same length
    # plt.subplot(122)
    # plt.title('Shifting process using time lag')
    # plt.plot(c_sig, ls='--', label='aligned data', c='C0')
    # plt.plot(SG_shift_scaled_1, label='Resampled smoothed internal data', c='C1')
    # plt.plot(SG_ref_7_1, label='Smoothed surrogate data', c='C2')
    # plt.legend(loc='best')

    # plt.show()