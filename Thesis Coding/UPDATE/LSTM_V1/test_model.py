__author__ = "Jakob Aungiers"
__copyright__ = "Jakob Aungiers 2018"
__version__ = "2.0.0"
__license__ = "MIT"

import os
import json
from math import sqrt
import matplotlib.pyplot as plt
from core.data_processor import DataLoader
from core.model import Model
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error
from scipy import spatial
from statsmodels.graphics.tsaplots import plot_acf

def euclidean_distance(x, y):
    # x actual data, y prediction
    return sqrt(sum(pow(a - b, 2) for a, b in zip(x, y)))

def RMSE(x, y):
    # x actual data, y prediction
    return sqrt(mean_squared_error(x, y))

def MAE(x, y):
    # x actual data, y prediction
    return mean_absolute_error(x, y)

def metrics_cal(true_data, predicted_data):
    pred_data_expand = np.expand_dims(predicted_data, axis=1)

    ED = euclidean_distance(true_data, pred_data_expand)
    CS = 1 - spatial.distance.cosine(true_data, predicted_data)
    RMSE_res = RMSE(true_data, predicted_data)
    MAE_res = MAE(true_data, predicted_data)

    print("The Euclidean distance is: %.4f" % ED)
    print("The cosine similarity is: %.4f" % CS)
    print("The RMSE is: %.4f" % RMSE_res)
    print("The MAE is: %.4f" % MAE_res)

def plot_results(predicted_data, true_data):

    fig = plt.figure()
    plt.plot(true_data, label='True Data')
    plt.plot(predicted_data, label='Prediction')
    plt.grid()
    plt.legend()
    plt.show()

def main():
    configs = json.load(open('config_test.json', 'r'))
    if not os.path.exists(configs['model']['save_dir']): os.makedirs(configs['model']['save_dir'])

    data = DataLoader(
        os.path.join('data', configs['data']['filename']),
        configs['data']['train_test_split'],
        configs['data']['columns']
    )

    filename = 'C:\\Users\\lappa\\Desktop\\UPDATE\\LSTM_V1\\saved_models\\recovery_regular_breath_holding-IJ_test-depth2668.h5'
    model = Model()
    model.load_model(filename)

    x_test, y_test = data.get_test_data(
        seq_len=configs['data']['sequence_length'],
        normalise=configs['data']['normalise']
    )

    predictions = model.predict_point_by_point(x_test)
    # metrics_cal(y_test, predictions)

    plot_acf(y_test)
    plt.show()
    # plot_results(predictions, y_test)

if __name__ == '__main__':
    main()