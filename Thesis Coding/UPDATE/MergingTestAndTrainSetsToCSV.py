# This script will merge the TRAIN and the TEST dataset into one CSV file with end _US.csv.
# INPUT: train: CSV dataset of training, test: CSV dataset of testing, filename: directory to save you the merged CSV.
# OUTPUT: merged CSV file.
# NOTE: it assumes that the breathing patterns are 8:  breath-holding, coughing, deep, recovery-regular/breath-holding,
# recovery regular-deep, recovery regular-shallow, regular and shallow. Moreover, the train and test CSV files are
# assumed to have 2 columns.
# column1: depth1 which stands for the most outer depth measurements.
# column 2: depth2668 which stands for the most inner depth from the US measurements.

import pandas as pd

def traintest2CSV(train, test, dir, breath_dict):

    for cnt in range(0, len(breath_dict)):
            a = pd.read_csv(train + breath_dict[cnt] + '\\' + 'TRAIN.csv', usecols=[0, 1])
            b = pd.read_csv(test + breath_dict[cnt] + '\\' + 'TEST.csv', usecols=[0, 1])

            if (cnt == 0 or cnt == 2 or cnt == 6 or cnt == 7):
                a = a[12004:len(a)]
                c = a[0:12004]
                a.to_csv( test + breath_dict[cnt] + '\\' + 'TEST3.csv', index=False)
                c.to_csv( test + breath_dict[cnt] + '\\' + 'TEST4.csv', index=False)

            elif (cnt == 1 or cnt == 3 or cnt == 4 or cnt == 5):
                a = a[3004:len(a)]
                c = a[0:3004]
                a.to_csv(test + breath_dict[cnt] + '\\' + 'TEST3.csv', index=False)
                c.to_csv(test + breath_dict[cnt] + '\\' + 'TEST4.csv', index=False)

            frames = [a, b]
            merged = pd.concat(frames)
            merged.to_csv(dir + breath_dict[cnt] +'_US.csv', index=False)

    for cnt in range(0, len(breath_dict)):
            a = pd.read_csv('C:\\Users\\user\\Desktop\\CSV_files\\' + breath_dict[cnt] + '_US.csv', usecols=[0, 1])
            b = pd.read_csv(test + breath_dict[cnt] + '\\' + 'TEST4.csv', usecols=[0, 1])

            frames = [a, b]
            merged = pd.concat(frames)
            merged.to_csv(dir + breath_dict[cnt] +'_US.csv', index=False)

    return 0

train_file = 'C:\\Users\\user\\Desktop\\UPDATE\\Train\\'
test_file = 'C:\\Users\\user\\Desktop\\UPDATE\\Test\\'
filename = 'C:\\Users\\user\\Desktop\\CSV_files\\'
breath_pat = ['breath_holding_intermittent', 'coughing_intermittent', 'deep', 'recovery_regular_breath_holding', 'recovery_regular_deep', 'recovery_regular_shallow', 'regular', 'shallow']

v = traintest2CSV(train_file, test_file, filename, breath_pat)