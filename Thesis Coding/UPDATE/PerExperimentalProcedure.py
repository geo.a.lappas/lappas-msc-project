import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pylab
from pandas.core.window import Rolling as rolling
from scipy.signal import find_peaks

# Implementation of algorithm from https://stackoverflow.com/a/22640362/6029703

def thresholding_algo(y, lag, threshold, influence):

    signals = np.zeros(len(y))
    filteredY = np.array(y)
    avgFilter = [0]*len(y)
    stdFilter = [0]*len(y)
    avgFilter[lag - 1] = np.mean(y[0:lag])
    stdFilter[lag - 1] = np.std(y[0:lag])
    for i in range(lag, len(y)):
        if abs(y[i] - avgFilter[i-1]) > threshold * stdFilter [i-1]:
            if y[i] > avgFilter[i-1]:
                signals[i] = 1
            else:
                signals[i] = -1

            filteredY[i] = influence * y[i] + (1 - influence) * filteredY[i-1]
            avgFilter[i] = np.mean(filteredY[(i-lag+1):i+1])
            stdFilter[i] = np.std(filteredY[(i-lag+1):i+1])
        else:
            signals[i] = 0
            filteredY[i] = y[i]
            avgFilter[i] = np.mean(filteredY[(i-lag+1):i+1])
            stdFilter[i] = np.std(filteredY[(i-lag+1):i+1])

    return dict(signals = np.asarray(signals),
                avgFilter = np.asarray(avgFilter),
                stdFilter = np.asarray(stdFilter))

name = ['ACartesian', 'Bcartesian', 'Ccartesian', 'Dcartesian', 'Ecartesian']
filename = 'C:\\Users\\lappa\\Documents\\Gerard script\\PerExperimentalProcedure\\'

a = pd.read_csv(filename + '\\' + name[0] + '.csv', usecols=[2667])

b = a[0:6000]
b1 = a[6000:12000]
b2 = a[13500:19500]
b3 = a[21000:27000]
b4 = a[28500:30000]

c = b.to_numpy()
c = np.squeeze(c, axis=1)

c1 = b1.to_numpy()
c1 = np.squeeze(c1, axis=1)
c2 = b2.to_numpy()
c2 = np.squeeze(c2, axis=1)
c3 = b3.to_numpy()
c3 = np.squeeze(c3, axis=1)
c4 = b4.to_numpy()
c4 = np.squeeze(c4, axis=1)

smooth_data = rolling(b, 5).mean() # this is the moving average filtering for smoothening the data
smooth_data = smooth_data.to_numpy()
smooth_data = np.squeeze(smooth_data, axis=1)

smooth_data1 = rolling(b1, 5).mean() # this is the moving average filtering for smoothening the data
smooth_data1 = smooth_data1.to_numpy()
smooth_data1 = np.squeeze(smooth_data1, axis=1)

smooth_data2 = rolling(b2, 5).mean() # this is the moving average filtering for smoothening the data
smooth_data2 = smooth_data2.to_numpy()
smooth_data2 = np.squeeze(smooth_data2, axis=1)

smooth_data3 = rolling(b3, 5).mean() # this is the moving average filtering for smoothening the data
smooth_data3 = smooth_data3.to_numpy()
smooth_data3 = np.squeeze(smooth_data3, axis=1)

smooth_data4 = rolling(b4, 5).mean() # this is the moving average filtering for smoothening the data
smooth_data4 = smooth_data4.to_numpy()
smooth_data4 = np.squeeze(smooth_data4, axis=1)

# detect peaks using Scipy 1st way
# peaks, _ = find_peaks(c, height=4.8535, distance=70)
peaks2, _2 = find_peaks(smooth_data, height=4.8535, distance=70)

peaks21, _21 = find_peaks(smooth_data1, height=4.8535, distance=70)
peaks22, _22 = find_peaks(smooth_data2, height=4.8535, distance=70)
peaks23, _23 = find_peaks(smooth_data3, height=4.8535, distance=70)
peaks24, _24 = find_peaks(smooth_data4, height=4.8535, distance=15)

threshold = 1.5
result1 = thresholding_algo(smooth_data1, 150, threshold, 0.5)
result3 = thresholding_algo(smooth_data2, 150, threshold, 0.5)

# kind of feature extraction - find a rough pattern between
plt.figure()
plt.title('Respiration peaks pattern')
plt.xlabel('Samples')
plt.ylabel('Magnitude (mm)')
# plt.scatter(peaks2, smooth_data[peaks2], color='C0')  # comment this if you want to plot the new feature without annotations
plt.plot(peaks2, smooth_data[peaks2], label='regular')
# plt.scatter(peaks21, smooth_data1[peaks21], color='C1')  # comment this if you want to plot the new feature without annotations
plt.plot(peaks21, smooth_data1[peaks21], label='breath-hold')
# # plt.plot(np.arange(1, len(smooth_data1)+1), result1["avgFilter"], label='bh_avg')
# plt.scatter(peaks22, smooth_data2[peaks22], color='C2')  # comment this if you want to plot the new feature without annotations
plt.plot(peaks22, smooth_data2[peaks22], label='shallow')
# plt.scatter(peaks23, smooth_data3[peaks23], color='C3')  # comment this if you want to plot the new feature without annotations
plt.plot(peaks23, smooth_data3[peaks23], label='deep')
# # plt.plot(np.arange(1, len(smooth_data3)+1), result3["avgFilter"], label='deep_avg')
# plt.scatter(peaks24, smooth_data4[peaks24], color='C4')  # comment this if you want to plot the new feature without annotations
plt.plot(peaks24, smooth_data4[peaks24], label='cough')
plt.legend()
plt.show()


# plt.figure()
# # plt.subplot(121)
# # plt.plot(c)
# # plt.plot(peaks, c[peaks], "x") # peaks in the normal signal
# # plt.subplot(122)
# plt.title('Peak Signal Detection #1 using find_peaks')
# plt.xlabel('Samples')
# plt.ylabel('Magnitude (mm)')
# plt.plot(smooth_data)
# plt.plot(peaks2, smooth_data[peaks2], "x")
# plt.show()

# # kind of feature extraction - find a rough pattern between
# plt.figure()
# plt.title('Cough respiration peak pattern')
# plt.xlabel('Samples')
# plt.ylabel('Magnitude (mm)')
# # plt.scatter(peaks24, smooth_data4[peaks24], color='C0', label='peaks') # comment this if you want to plot the new feature without annotations
# # plt.plot(smooth_data4,  label='original signal')
# plt.plot(peaks24, smooth_data4[peaks24], label='cough') # 'x'
# plt.legend()
# plt.show()

# 2nd way
# y = a[21000:27000].to_numpy() # needs to be changed
# threshold = 1.5
# result = thresholding_algo(y, 150, threshold, 0.5)
#
# # Plot result
# plt.figure()
# pylab.subplot(211)
# pylab.plot(np.arange(1, len(y)+1), y)
# plt.title('Signal over time')
# # plt.xlabel('Samples')
# plt.ylabel('Magnitude (mm)')
# # pylab.plot(np.arange(1, len(y)+1), result["avgFilter"], color="cyan", lw=2)
# # pylab.plot(np.arange(1, len(y)+1), result["avgFilter"] + threshold * result["stdFilter"], color="green", lw=2)
# # pylab.plot(np.arange(1, len(y)+1), result["avgFilter"] - threshold * result["stdFilter"], color="green", lw=2)
# pylab.subplot(212)
# pylab.step(np.arange(1, len(y)+1), result["signals"], color="red", lw=2)
# plt.title('Peak Signal Detection #2 using dispersion')
# plt.xlabel('Samples')
# plt.ylabel('Phase')
# pylab.ylim(-1.5, 1.5)
# pylab.show()