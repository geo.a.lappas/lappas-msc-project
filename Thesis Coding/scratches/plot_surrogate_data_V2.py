import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.signal import savgol_filter as savgol
from scipy.signal import medfilt as medfilt_sc

name = ['Acartesian', 'Bcartesian', 'Ccartesian', 'Dcartesian', 'Ecartesian']
filename = 'C:\\Users\\lappa\\Desktop\\ThesisData\\SurrogateSignal'

_arr_origin = []
_arr = []
_arr_med = []
_mean = []
_std = []

for n in range(len(name)):

    a = pd.read_csv(filename + '\\' + name[n] + '.csv', usecols=[0])
    # a = a[0:6000] # taking only the regular breathing pattern
    df = a['depth2667']
    d = df.to_numpy()
    SG = savgol(d, 51, 7)
    median_filt = medfilt_sc(d, 99)
    # SG = np.expand_dims(SG, axis=1)
    _arr_origin.append(d)
    _arr.append(SG)
    _arr_med.append(median_filt)
    _mean.append(np.mean(SG))
    _std.append(np.std(SG))

    # fs = 10e3
    #
    # f, Pxx_den = signal.periodogram(SG, fs)
    # plt.semilogy(f, Pxx_den)
    # plt.ylim([1e-7, 1e2])
    # # plt.ylim([1e-7, 1e2])
    # plt.xlabel('frequency [Hz]')
    # plt.ylabel('PSD [V**2/Hz]')
    # plt.show()

# len_f = _arr[4].shape[0]
#
# data_arr = np.concatenate((_arr[0][0:len_f], _arr[2][0:len_f], _arr[3][0:len_f], _arr[4][0:len_f]), axis=1)
# mean_signal = np.average(data_arr, axis=1)

# d_d = {'A': _arr[0][0:len_f], 'B': _arr[1][0:len_f], 'C': _arr[2][0:len_f], 'D': _arr[3][0:len_f], 'E': _arr[4][0:len_f]}
# data_f = pd.DataFrame(data=d_d)

# data_f.plot.box(vert=False, color='black', sym='r+')
# plt.xlabel('US magnitude in mm')
# plt.ylabel('# of Subject')
# plt.show()


# freqs_A, psd_A = signal.welch(_arr[0])
# freqs_C, psd_C = signal.welch(_arr[2])
# freqs_D, psd_D = signal.welch(_arr[3])
# freqs_E, psd_E = signal.welch(_arr[4])
#
# freqs_normA = (freqs_A - min(freqs_A)) / (max(freqs_A) - min(freqs_A))
# psd_normA = (psd_A - min(psd_A)) / (max(psd_A) - min(psd_A))
# freqs_normC = (freqs_C - min(freqs_C)) / (max(freqs_C) - min(freqs_C))
# psd_normC = (psd_C - min(psd_C)) / (max(psd_C) - min(psd_C))
# freqs_normD = (freqs_D - min(freqs_D)) / (max(freqs_D) - min(freqs_D))
# psd_normD = (psd_D - min(psd_D)) / (max(psd_D) - min(psd_D))
# freqs_normE = (freqs_E - min(freqs_E)) / (max(freqs_E) - min(freqs_E))
# psd_normE = (psd_E - min(psd_E)) / (max(psd_E) - min(psd_E))

# plt.figure()
# plt.plot(freqs_normA, psd_normA)
# plt.plot(freqs_normC, psd_normC)
# plt.plot(freqs_normD, psd_normD)
# plt.plot(freqs_normE, psd_normE)
# plt.xlim([0, 1])
# plt.title('Normalized PSD')
# plt.xlabel('Frequency')
# plt.ylabel('Power')
# plt.show()


# print("Stop")
# mean_signal = _arr[]

plt.figure()
plt.title('Surrogate signal - complete experiment - Subject A')
plt.xlabel('# samples')
plt.ylabel('Magnitude (mm)')
# plt.plot(df.to_numpy(), c='C1')
plt.plot(_arr[0], c='C0', label='denoised SG')
# plt.plot(_arr_origin[0], c='C1', label='original signal')
plt.plot(_arr_med[0], c='C2', label='denoised median filt')
# plt.plot(mean_signal, c='C0', label='average of rest of subjects')
# plt.plot(_arr[1], c='C1', ls='--', label='Subject B')
# plt.plot(_arr[2], c='C2', label='Subject C')
# plt.plot(_arr[3], c='C3', label='Subject D', ls='--')
# plt.plot(_arr[4], c='C4', label='Subject E')
plt.legend()
plt.show()

print("Subject A: mean", _mean[0], "std", _std[0])
print("Subject B: mean", _mean[1], "std", _std[1])
print("Subject C: mean", _mean[2], "std", _std[2])
print("Subject D: mean", _mean[3], "std", _std[3])
print("Subject E: mean", _mean[4], "std", _std[4])

