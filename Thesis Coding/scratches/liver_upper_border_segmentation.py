# In this script, we will use Ellipse bounding box, morpholgical operations, fitlering and finally,
# Contouring methods to segment the upper border of the liver in MRI images.

# Settings for subject A: (75, 90), (60, 55)
# Settings for subject B: (80, 85), (50, 45)
# Settings for subject C: (90, 90), (55, 50)
# Settings for subject D: (90, 90), (65, 55)
# Settings for subject E: (75, 80), (55, 50)

import numpy as np
import cv2 as cv
from PIL import Image, ImageEnhance
import matplotlib.pyplot as plt
import os
import scipy.ndimage as ndimage
from scipy.interpolate import interp1d
import csv

name_f = ["A_C", "B_C", "C_C", "D_C", "E_C"]
search = "C:\\Users\\lappa\\Desktop\\PNG_160\\PNG_" # pick subject file
new_save_path = "C:\\Users\\lappa\\Desktop\\"

# for cnt_n in range(len(name_f)):
#     new_name = search + name_f[cnt_n]
#     print(new_name)

for cnt_n in range(0,len(name_f)):
    new_name = search + name_f[cnt_n]
    list = sorted(os.listdir(new_name), key=len)  # dir is your directory path

    st = int(list[0].split('.png')[0].split('I')[1])  # start of for loop
    en = int(list[-1].split('.png')[0].split('I')[1])  # end of for loop
    cnt = st
    img_array = []
    min_y = []
    min_x = []
    min_y_scaled = [] # in mm displacement

    for i in range(st, en + 1, 1):

        if i <= 99: im = Image.open(new_name + '\\I' + str(i) + '.png').convert('L')
        else: im = Image.open(new_name + '\\I' + str(i) + '.png').convert('L')

        mask = np.zeros(shape=(160, 160), dtype=np.uint8)
        label_m = mask.copy()

        if cnt_n==0:  new_img = cv.ellipse(mask, (75, 90), (60, 55), 90, 0, 360, 255, -1) # change this based on the subject
        elif cnt_n==1: new_img = cv.ellipse(mask, (80, 85), (50, 45), 90, 0, 360, 255, -1)
        elif cnt_n==2: new_img = cv.ellipse(mask, (90, 90), (55, 50), 90, 0, 360, 255, -1)
        elif cnt_n==3: new_img = cv.ellipse(mask, (90, 90), (65, 55), 90, 0, 360, 255, -1)
        elif cnt_n==4: new_img = cv.ellipse(mask, (75, 80), (55, 50), 90, 0, 360, 255, -1)

        im2, contours0, _ = cv.findContours(new_img.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        # im = (im / 256).astype('uint8') # convert from 32bit to 8bit
        img = np.array(im)

        # cv.drawContours(label_m, contours0, -1, 255) # contour only
        cv.drawContours(label_m, contours0, -1, 255, thickness=cv.FILLED) # filled contour

        # Apply the mask and display the result - masked liver image
        maskedImg = cv.bitwise_and(src1=img, src2=new_img)

        # plt.figure()
        # plt.subplot(131)
        # plt.imshow(new_img, cmap=plt.cm.bone)
        # plt.title('Ellipses mask')
        # plt.subplot(132)
        # plt.imshow(img, cmap=plt.cm.bone)
        # plt.imshow(label_m, cmap=plt.cm.bone, alpha=0.5)
        # plt.figure(133)
        # plt.imshow(maskedImg, cmap=plt.cm.bone)
        # # plt.title('Ellipses overlay MRI image')
        # plt.title('Masked image containing only liver')
        # plt.show()
    ##########################################

        maskedImg2 = Image.fromarray(np.uint8(maskedImg))
        obj = ImageEnhance.Contrast(maskedImg2).enhance(70)  # contrast enhancement
        obj_changed = np.array(obj)  # Convert PIL to NP array

        im21, contours01,_1 = cv.findContours(obj_changed.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)  # hierarchy
        c_sorted = sorted(contours01, key=cv.contourArea, reverse=True) # sort the found contours
        mask1 = np.zeros(shape=(maskedImg.shape[0], maskedImg.shape[1]), dtype=np.uint8)
        cv.drawContours(mask1, c_sorted[:2], -1, 255) # thickness=cv.FILLED) # take the 3 largest

        kernel = np.ones((2, 2), np.uint8)  # Taking a matrix of size 3 as the kernel

        # this should be enabled
        img_dilation = cv.dilate(mask1, kernel, iterations=1)
        # img_dilation = mask1

        GF_img = ndimage.gaussian_filter(img_dilation, sigma=0.5, order=0) # if I need without dilation, just pass mask1 as argument
        # linear_interpolation = interp1d(img_dilation[0], img_dilation[0])
        # f2 = interp1d(x, y, kind='cubic')

        # this mask is used for discarding all the image info except of the upper border contour
        mask2 = np.zeros(shape=(maskedImg.shape[0], maskedImg.shape[1]), dtype=np.uint8)
        # mask2[0:80, 50:110] = 1 # mask with size (80,60) using center (80,40)
        mask2[0:100, 30:115] = 1

        final_maskedImg = cv.bitwise_and(src1=img_dilation, src2=mask2)

        # # print('stop')
        # plt.figure()
        # plt.subplot(141)
        # plt.title('Masked Liver image %d' % i)
        # plt.imshow(maskedImg, cmap=plt.cm.bone)
        # plt.subplot(142)
        # plt.title('Contour of image %d' % i)
        # # plt.title('Linear interpolation image %d' % i)
        # plt.imshow(mask1, cmap=plt.cm.bone) # mask
        # plt.subplot(143)
        # plt.title('Dilated Gaussian filtered image %d' % i)
        # plt.imshow(GF_img, cmap=plt.cm.bone)
        # plt.subplot(144)
        # nearest = plt.imshow(mask1, interpolation='nearest', cmap=plt.cm.bone)
        # plt.title('Filtered using KNN image %d' % i)
        # plt.show()

        img_dilation = img_dilation.astype('float64')
        img_dilation = img_dilation/255.0

        y_points = np.where(img_dilation == 1)[0]
        x_points = np.where(img_dilation == 1)[1]

        min_y.append(min(np.where(img_dilation == 1)[0]))
        min_x.append(min(np.where(img_dilation == 1)[1]))

        min_y_scaled.append(float(min(np.where(img_dilation == 1)[0])*1.875))

    # start_point = min_y[0]
    # min_y = [x_sub - start_point for x_sub in min_y]
    start_point_scaled = min_y_scaled[0] # this is the zero reference value
    min_y_scaled = [x_sub_sc - start_point_scaled for x_sub_sc in min_y_scaled]

    # np.savetxt(new_save_path + name_f[cnt_n] + '_internal_motion.csv', min_y_scaled, delimiter=",", fmt='%s')

    x_lin = np.arange(0, en + 1 - st, 1)

    # plt.figure()
    # plt.plot(x_lin, min_y, c='C1')
    # plt.title('Subject D liver upper border motion in SI-direction')
    # plt.xlabel('# of sample')
    # plt.ylabel('Pixel position in SI direction')
    # plt.show()
    # #
    # plt.figure()
    # plt.plot(x_lin, min_y_scaled, c='C2')
    # plt.title('Subject D liver upper border motion in SI-direction')
    # plt.xlabel('# of sample')
    # plt.ylabel('Displacement (mm) in SI direction')
    # # plt.ylim(-15, 25)
    # plt.show()

    plt.figure()
    # plt.subplot(121)
    # plt.title('Masked Liver image %d' % i)
    # plt.imshow(maskedImg, cmap=plt.cm.bone)
    # plt.subplot(122)
    plt.imshow(img, cmap=plt.cm.bone)
    plt.imshow(final_maskedImg, cmap=plt.cm.bone, alpha=0.5)
    plt.title('MRI image with upper border segmentation')
    plt.show()