import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.signal import savgol_filter as savgol
from sklearn import preprocessing

def equalize_array_size(array1,array2,cut_m=1):
    '''
    reduce the size of one sample to make them equal size.
    The sides of the biggest signal are truncated

    Args:
        array1 (1d array/list): signal for example the reference
        array2 (1d array/list): signal for example the target

    Returns:
        array1 (1d array/list): middle of the signal if truncated
        array2 (1d array/list): middle of the initial signal if there is a size difference between the array 1 and 2
        dif_length (int): size diffence between the two original arrays
    '''

    len1, len2 = len(array1), len(array2)
    dif_length = len1-len2
    if dif_length<0:
        if cut_m == 1: array2 = array2[int(np.floor(-dif_length/2)):len2-int(np.ceil(-dif_length/2))]
        else: array2 = array2[0:len2 - int(np.ceil(-dif_length))]
    elif dif_length>0:
        if cut_m == 1: array1 = array1[int(np.floor(dif_length/2)):len1-int(np.ceil(dif_length/2))]
        else: array1 = array1[0:len1 - int(np.ceil(dif_length))]

    return array1, array2

name = ['Resampled A_internal_motion', 'Resampled B_internal_motion', 'Resampled C_internal_motion', 'Resampled D_internal_motion', 'Resampled E_internal_motion']
filename = 'C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\'

_arr = []
superior_mean = []
superior_std = []
superior_var = []
inferior_mean = []
inferior_std = []
inferior_var = []
SG_com = []

for n in range(len(name)):
    a = pd.read_csv(filename + name[n] + '.csv', usecols=[0])
    df = a['displacement']
    d = df.to_numpy()
    # d = d[0:5000]
    # SG = savgol(d, 21, 4)
    SG = d
    x = np.linspace(0,SG.shape[0],SG.shape[0])
    y = np.zeros(shape=(x.shape[0]))

    # print(x)
    diff_a = np.diff(np.sign(np.diff(SG))).nonzero()[0] + 1  # local min & max - detect non zero points
    diff_b = (np.diff(np.sign(np.diff(SG))) > 0).nonzero()[0] + 1  # local min - inferior
    diff_c = (np.diff(np.sign(np.diff(SG))) < 0).nonzero()[0] + 1  # local max - superior
    # +1 due to the fact that diff reduces the original index number

    # plot
    # plt.figure(figsize=(12, 5))
    # plt.plot(x, SG, color='grey', ls='--', label="Upper border liver motion")
    # plt.plot(x, y, color='black')
    # plt.plot(x[diff_b], SG[diff_b], "o", label="min-inferior", color='r')
    # plt.plot(x[diff_c], SG[diff_c], "o", label="max-superior", color='b')
    # # plt.plot(x[diff_b], SG[diff_total], label="superior-inferior", color='g')
    # plt.title('Upper liver border motion over time displayed in mm')
    # plt.xlabel('number of samples')
    # plt.ylabel('Displacement of the liver in SI-direction')
    # plt.legend(loc='best')
    # plt.show()

    SG_com.append(SG)
    superior_mean.append(np.mean(SG[diff_c]))
    inferior_mean.append(np.mean(SG[diff_b]))
    superior_std.append(np.std(SG[diff_c]))
    inferior_std.append(np.std(SG[diff_b]))
    superior_var.append(np.var(SG[diff_c]))
    inferior_var.append(np.var(SG[diff_b]))

SG_min = []
SG_max = []
SG_avg = []

for m in range(len(name)):
    SG_min = min(SG_com[m])
    SG_max = max(SG_com[m])
    SG_avg = np.mean(SG_com[m])

    x_avg = np.linspace(0,SG_com[m].shape[0],SG_com[m].shape[0])
    y_avg = np.zeros(shape=(SG_com[m].shape[0]))
    y_avg[:] = SG_avg
    y_min = np.zeros(shape=(SG_com[m].shape[0]))
    y_min[:] = SG_min
    y_max = np.zeros(shape=(SG_com[m].shape[0]))
    y_max[:] = SG_max

    # plt.figure()
    # plt.title('All subjects liver motion - normal breathing')
    # plt.xlabel('# samples')
    # plt.ylabel('SI displacement (mm)')
    # # plt.plot(df.to_numpy(), c='C1')
    # plt.plot(SG_com[m], c='C0', label='Subject A')
    # plt.plot(x_avg, y_avg, color='black')
    # plt.plot(x_avg, y_min, color='green')
    # plt.plot(x_avg, y_max, color='green')
    # plt.legend()
    # plt.show()

    print("Avg of", m, "-subject:", SG_avg, "min/max:", SG_min, SG_max)

# len_f = SG_com[4].shape[0]
# SG_com[4], SG_com[0] = equalize_array_size(SG_com[4], SG_com[0])
# SG_com[4], SG_com[1] = equalize_array_size(SG_com[4], SG_com[1])
# SG_com[4], SG_com[2] = equalize_array_size(SG_com[4], SG_com[2])
# SG_com[4], SG_com[3] = equalize_array_size(SG_com[4], SG_com[3])
#
# d_d = {'A': SG_com[0], 'B': SG_com[1], 'C': SG_com[2], 'D': SG_com[3], 'E': SG_com[4]}
# data_f = pd.DataFrame(data=d_d)

# data_f.plot.box(vert=False, color='black', sym='r+')
# plt.xlabel('Liver SI-position in mm')
# plt.ylabel('# of Subject')
# plt.show()

freqs_A, psd_A = signal.welch(SG_com[0])
freqs_C, psd_C = signal.welch(SG_com[2])
freqs_D, psd_D = signal.welch(SG_com[3])
freqs_E, psd_E = signal.welch(SG_com[4])

freqs_normA = (freqs_A - min(freqs_A)) / (max(freqs_A) - min(freqs_A))
psd_normA = (psd_A - min(psd_A)) / (max(psd_A) - min(psd_A))
freqs_normC = (freqs_C - min(freqs_C)) / (max(freqs_C) - min(freqs_C))
psd_normC = (psd_C - min(psd_C)) / (max(psd_C) - min(psd_C))
freqs_normD = (freqs_D - min(freqs_D)) / (max(freqs_D) - min(freqs_D))
psd_normD = (psd_D - min(psd_D)) / (max(psd_D) - min(psd_D))
freqs_normE = (freqs_E - min(freqs_E)) / (max(freqs_E) - min(freqs_E))
psd_normE = (psd_E - min(psd_E)) / (max(psd_E) - min(psd_E))

# psd_norm = (psd - np.mean(psd)) / (np.std(psd))

# freqs_norm = preprocessing.normalize([freqs])
# psd_norm = preprocessing.normalize([psd])

# plt.figure(figsize=(5, 4))
plt.figure()
# plt.subplot(121)
# plt.plot(freqs, psd)
# # plt.semilogx(freqs, psd)
# plt.title('PSD: power spectral density')
# plt.xlabel('Frequency')
# plt.ylabel('Power')
# plt.subplot(122)
plt.plot(freqs_normA, psd_normA)
plt.plot(freqs_normC, psd_normC)
plt.plot(freqs_normD, psd_normD)
plt.plot(freqs_normE, psd_normE)
# plt.semilogx(freqs, psd)
plt.xlim([0, 1])
plt.title('Normalized PSD')
plt.xlabel('Frequency')
plt.ylabel('Power')
# plt.tight_layout()
plt.show()

plt.figure()
plt.title('Subjects A liver motion - normal breathing')
# plt.title('Subjects B,D liver motion - normal breathing')
plt.xlabel('# samples')
plt.ylabel('SI displacement (mm)')
plt.plot(SG_com[0], c='C0', ls='--', label='Subject A')
plt.plot(SG_com[2], c='C1', label='Subject C')
plt.plot(SG_com[4], c='C2', label='Subject E')
plt.plot(SG_com[1], c='C3', label='Subject B')
plt.plot(SG_com[3], c='C4', label='Subject D')
plt.legend()
plt.show()

total_var_A = superior_var[0] + inferior_var[0]
total_std_A = np.sqrt(np.sum(total_var_A))
total_var_B = superior_var[1] + inferior_var[1]
total_std_B = np.sqrt(np.sum(total_var_B))
total_var_C = superior_var[2] + inferior_var[2]
total_std_C = np.sqrt(np.sum(total_var_C))
total_var_D = superior_var[3] + inferior_var[3]
total_std_D = np.sqrt(np.sum(total_var_D))
total_var_E = superior_var[4] + inferior_var[4]
total_std_E = np.sqrt(np.sum(total_var_E))

A = superior_mean[0] - inferior_mean[0]
B = superior_mean[1] - inferior_mean[1]
C = superior_mean[2] - inferior_mean[2]
D = superior_mean[3] - inferior_mean[3]
E = superior_mean[4] - inferior_mean[4]

print('Subject A:', format(A, '.2f'), '\u00B1', format(total_std_A, '.2f'))
print('Subject B:', format(B, '.2f'), '\u00B1', format(total_std_B, '.2f'))
print('Subject C:', format(C, '.2f'), '\u00B1', format(total_std_C, '.2f'))
print('Subject D:', format(D, '.2f'), '\u00B1', format(total_std_D, '.2f'))
print('Subject E:', format(E, '.2f'), '\u00B1', format(total_std_E, '.2f'), "\n")

print("Subject A-superior:", format(superior_mean[0], '.2f'), "\u00B1", format(superior_std[0], '.2f'))
print("Subject B-superior:", format(superior_mean[1], '.2f'), "\u00B1", format(superior_std[1], '.2f'))
print("Subject C-superior:", format(superior_mean[2], '.2f'), "\u00B1", format(superior_std[2], '.2f'))
print("Subject D-superior:", format(superior_mean[3], '.2f'), "\u00B1", format(superior_std[3], '.2f'))
print("Subject E-superior:", format(superior_mean[4], '.2f'), "\u00B1", format(superior_std[4], '.2f'), "\n")
print("Subject A-inferior:", format(inferior_mean[0], '.2f'), "\u00B1", format(inferior_std[0], '.2f'))
print("Subject B-inferior:", format(inferior_mean[1], '.2f'), "\u00B1", format(inferior_std[1], '.2f'))
print("Subject C-inferior:", format(inferior_mean[2], '.2f'), "\u00B1", format(inferior_std[2], '.2f'))
print("Subject D-inferior:", format(inferior_mean[3], '.2f'), "\u00B1", format(inferior_std[3], '.2f'))
print("Subject E-inferior:", format(inferior_mean[4], '.2f'), "\u00B1", format(inferior_std[4], '.2f'))