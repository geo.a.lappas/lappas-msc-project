# Linear Regression script fot 2 vectors using Scikit learn packages

import numpy as np
import pandas as pd
from scipy.signal import savgol_filter as savgol
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from random import randrange
import matplotlib.pyplot as plt

def array_equal_size(arr_1, arr_2, cut_m=1):
    '''
    Reduce the size of one sample to make them equal size. The sides of the biggest signal are truncated

    Args:
        arr_1 (1d array/list): signal for example the reference & arr_2 (1d array/list): signal for example the target
        cut_m (int): cutting mode either 1 or 2
    Returns:
        if cut_m == 1: cutting the larger signal at the beginning and end of the using half of the difference at each part
            arr_1 (1d array/list) & arr_2 (1d array/list) - cutted versions
        if cut_m == 2: cutting the larger signal at the end of using the difference between the two signals
            arr_1 (1d array/list) & arr_2 (1d array/list) - cutted versions
        dif_length (int): size diffence between the two original arrays
    '''

    len_1, len_2 = len(arr_1), len(arr_2)
    dif_length = len_1-len_2

    if dif_length<0:
        if cut_m == 1: arr_2 = arr_2[int(np.floor(-dif_length/2)):len_2-int(np.ceil(-dif_length/2))]
        else: arr_2 = arr_2[0:len_2 - int(np.ceil(-dif_length))]
    elif dif_length>0:
        if cut_m == 1: arr_1 = arr_1[int(np.floor(dif_length/2)):len_1-int(np.ceil(dif_length/2))]
        else: arr_1 = arr_1[0:len_1 - int(np.ceil(dif_length))]

    return arr_1, arr_2, dif_length

def cross_validation_split(dataset, n_folds):

    dataset_split = list()
    dataset_copy = list(dataset)
    fold_size = int(len(dataset) / n_folds)

    for i in range(n_folds):
        fold = list()
        while len(fold) < fold_size:
            index = randrange(len(dataset_copy))
            fold.append(dataset_copy.pop(index))
        dataset_split.append(fold)

    return dataset_split

if __name__ == "__main__":
    a = pd.read_csv('C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\Resampled E_internal_motion.csv', usecols=[0])
    df_a = a['displacement']
    d_a = df_a.to_numpy()

    b = pd.read_csv('C:\\Users\\lappa\\Desktop\\ThesisData\\SurrogateSignal\\Ecartesian.csv', usecols=[0])
    df_b = b['depth2667']
    d_b = df_b.to_numpy()

    ref_signal = d_b # US signal - pulse receiver magnitude
    shift_signal = d_a # MRI signal - displacement (mm) in SI

    # smoothing process
    SG_ref_7 = savgol(ref_signal, 51, 7)
    SG_shift = savgol(shift_signal, 21, 4)

    # # Scaling based on Reference signal - surrogate
    # mRef = np.mean(SG_ref_7)
    # stdRef = np.std(SG_ref_7)
    # mSig = np.mean(SG_shift)
    # stdSig = np.std(SG_shift)
    # SG_shift_scaled = ((SG_shift - mSig) / stdSig) * stdRef + mRef

    # Scaling based on Reference signal - internal motion
    mRef = np.mean(SG_ref_7)
    stdRef = np.std(SG_ref_7)
    mSig = np.mean(SG_shift)
    stdSig = np.std(SG_shift)
    SG_ref_7 = ((SG_ref_7 - mRef) / stdRef) * stdSig + mSig
    SG_shift_scaled = SG_shift

    # create same size for internal motion and surrogate signals - cutting difference at the end of signal
    SG_ref_7_1, SG_shift_scaled_1, dif_length = array_equal_size(SG_ref_7, SG_shift_scaled, 2)

    # normalization of the signals
    SG_ref_7_norm = (SG_ref_7_1 - np.mean(SG_ref_7_1)) / (np.std(SG_ref_7_1))
    SG_shift_scaled_norm = (SG_shift_scaled_1 - np.mean(SG_shift_scaled_1)) / (np.std(SG_shift_scaled_1))

    # 10-fold cross validation
    surrogate_split = cross_validation_split(SG_ref_7_norm, 10)
    internal_motion_split = cross_validation_split(SG_shift_scaled_norm, 10)

    MAE = []
    MSE = []
    RMSE = []
    y_pred = []

    for cnt_cv in range (0, len(surrogate_split)):

        # define lists
        surrogate_train=[]
        internal_motion_train=[]

        # define test samples
        y_test = surrogate_split[cnt_cv] # surrogate data test
        X_test = internal_motion_split[cnt_cv] # internal motion data test

        if cnt_cv > 0:
            for cnt_internal_1 in range(0, cnt_cv):
                surrogate_train.append(surrogate_split[cnt_internal_1])
                internal_motion_train.append(surrogate_split[cnt_internal_1])

            for cnt_internal_2 in range(cnt_cv+1, len(surrogate_split)):
                surrogate_train.append(surrogate_split[cnt_internal_2])
                internal_motion_train.append(internal_motion_split[cnt_internal_2])

        elif cnt_cv == 0:
            surrogate_train.append(surrogate_split[cnt_cv+1:len(surrogate_split)])
            internal_motion_train.append(internal_motion_split[cnt_cv+1:len(surrogate_split)])

        y_train = surrogate_train
        X_train = internal_motion_train
        # dtype=np.float64
        X_train = np.squeeze(np.asarray(X_train)).T
        X_test = np.squeeze(np.asarray(X_test))
        y_train = np.squeeze(np.asarray(y_train)).T
        y_test = np.squeeze(np.asarray(y_test))

        # Expand dimensions to work with regression function
        X_train = np.concatenate((X_train[:, :]), axis=0)
        y_train = np.concatenate((y_train[:, :]), axis=0)

        X_test = np.expand_dims(X_test, axis=1)
        y_test = np.expand_dims(y_test, axis=1)
        X_train = np.expand_dims(X_train, axis=1)
        y_train = np.expand_dims(y_train, axis=1)

        # Train the linear regression model
        regressor = LinearRegression()
        regressor.fit(X_train, y_train)  # training the algorithm

        # Make predictions
        y_pred.append(regressor.predict(X_test))

        MAE.append(metrics.mean_absolute_error(y_test, np.asarray(y_pred)[cnt_cv]))
        MSE.append(metrics.mean_squared_error(y_test, np.asarray(y_pred)[cnt_cv]))
        RMSE.append(np.sqrt(metrics.mean_squared_error(y_test, np.asarray(y_pred)[cnt_cv])))

        # Print results - MAE, MSE, RMSE
        # print('Mean Absolute Error fold', cnt_cv, ':', metrics.mean_absolute_error(y_test, np.asarray(y_pred)[cnt_cv]))

        # print('Mean Squared Error fold', cnt_cv, ':', metrics.mean_squared_error(y_test, np.asarray(y_pred)[cnt_cv]))

        # print('Root Mean Squared Error fold', cnt_cv, ':', np.sqrt(metrics.mean_squared_error(y_test, np.asarray(y_pred)[cnt_cv])))
        # print('\n')

    MAE_mean = np.mean(MAE)
    RMSE_mean = np.mean(RMSE)
    MAE_std = np.std(MAE)
    RMSE_std = np.std(RMSE)

    print('MAE is:', format(MAE_mean, '.2f'), "\u00B1", format(MAE_std, '.2f'))
    print('RMSE is:', format(RMSE_mean, '.2f'), "\u00B1", format(RMSE_std, '.2f'))

        # plt.figure()
        # plt.title('Actual liver motion and estimated motion')
        # plt.plot(y_test, label='Actual liver motion', c='C0')
        # plt.plot(y_pred[cnt_cv], ls='--', label='Estimated internal motion', c='C1')  # ls='--'
        # plt.legend(loc='best')
        # plt.show()