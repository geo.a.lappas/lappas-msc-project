import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
from PIL import Image

mask = np.zeros(shape=(160, 160), dtype=np.uint8)
label_m = mask.copy()

# new_img = cv.ellipse(mask,(77,83),(47,47),90,0,360,255,-1) # normal representation
new_img = cv.ellipse(mask,(83,75),(34,38),90,0,360,255,-1) # small representation
# new_img = cv.ellipse(mask,(83,82),(30,34),90,0,360,255,-1) @ works for motion
im2, contours0, _ = cv.findContours(new_img.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

search = "C:\\Users\\lappa\\Desktop\\PNG_160\\PNG_B_C\\"
im = Image.open(search + 'I3699.png')
img = np.array(im)

cv.drawContours(label_m, contours0, -1, 255)

plt.figure()
plt.subplot(121)
plt.imshow(new_img, cmap = plt.cm.bone)
plt.title('Ellipses mask')
plt.subplot(122)
plt.imshow(img, cmap = plt.cm.bone)
plt.imshow(label_m, cmap = plt.cm.bone, alpha=0.5)
plt.title('Ellipses overlay MRI image')
plt.show()
