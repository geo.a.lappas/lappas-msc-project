# Resampling signal using FFT. The number of samples after resampling is chosen based on the available surrogate data.

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import resample

name = ['A_C_internal_motion', 'B_C_internal_motion', 'C_C_internal_motion', 'D_C_internal_motion', 'E_C_internal_motion']
filename = 'C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal'

_arr = []
res_arr = []

for n in range(len(name)):

    if n == 0:
        cnt = 39
        s_name = 'A'
    elif n == 1:
        cnt = 38
        s_name = 'B'
    elif n == 2:
        cnt = 37
        s_name = 'C'
    elif n == 3:
        cnt = 37
        s_name = 'D'
    elif n == 4:
        cnt = 35
        s_name = 'E'

    a = pd.read_csv(filename + '\\' + name[n] + '.csv', usecols=[0])
    df = a['displacement']
    d = df.to_numpy()

    if n == 0: # because subject A has 800 MRI images
        res_signal = resample(d, cnt*773)
    else:
        res_signal = resample(d, cnt * 840)

    _arr.append(d)
    res_arr.append(res_signal)

    np.savetxt('C:\\Users\\lappa\\Desktop\\Resampled '+ s_name +'_internal_motion.csv', res_arr[n], delimiter=",", fmt='%s')

    # plt.figure()
    # plt.subplot(121)
    # plt.title('Internal motion - pixel position in SI-direction')
    # plt.xlabel('# samples')
    # plt.ylabel('Pixel position')
    # plt.plot(_arr[n], c='C0', label='Subject ' + s_name)
    # plt.legend()
    # plt.subplot(122)
    # plt.title('Internal motion - resampled ' + str(cnt) + ' times faster')
    # plt.xlabel('# samples')
    # plt.ylabel('Pixel position')
    # plt.plot(res_arr[n], c='C1', label='Subject '+ s_name)
    # plt.legend()
    # plt.show()