# Contrast and brightness enhancement operations. Along with some morphological operations.
# This script is intended to enhance the detection of the upper border of liver.

import numpy as np
import cv2 as cv
from PIL import Image, ImageEnhance
import matplotlib.pyplot as plt

search = "C:\\Users\\lappa\\Desktop\\PNG_160\\PNG_A_C\\"
im = Image.open(search + 'I182.png')

obj = ImageEnhance.Contrast(im).enhance(70)  # contrast enhancement
obj_changed = np.array(obj)  # Convert PIL to NP array
# result = np.where((obj_changed < 250))
# w = obj_changed.copy()
# w[result[0], result[1]] = 0
# w = w.astype('float64')
# w = w/255.0

# center
circles = cv.HoughCircles(obj_changed, cv.HOUGH_GRADIENT, 1, 50, param1=50, param2=30, minRadius=0, maxRadius=0)
circles = np.uint16(np.around(circles))

for i in circles[0, :]:
    #	draw	the	outer	circle
    cv.circle(obj_changed, (i[0], i[1]), i[2], (0, 255, 0), 6)
    #	draw	the	center	of	the	circle
    cv.circle(obj_changed, (i[0], i[1]), 2, (0, 0, 255), 3)

plt.figure()
plt.imshow(obj_changed, cmap=plt.cm.bone)
plt.show()



# kernel = np.ones((2, 2), np.uint8)  # Taking a matrix of size 5 as the kernel
# img_erosion = cv.erode(w, kernel, iterations=2)
# # kernel01 =  np.ones((1, 1), np.uint8)
# img_dilation = cv.dilate(img_erosion, kernel, iterations=3)
# img_dilation = img_dilation.astype('uint8')
#
# im2, contours0, _ = cv.findContours(img_dilation.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)  # hierarchy
# c = sorted(contours0, key=cv.contourArea, reverse=True) # c = max(contours0, key = cv.contourArea)
# mask1 = np.zeros(shape=(img_dilation.shape[0], img_dilation.shape[1]), dtype=np.uint8)
# mask2 = np.zeros(shape=(img_dilation.shape[0], img_dilation.shape[1]), dtype=np.uint8)
# cv.drawContours(mask1, contours0, -1, 255)
# cv.drawContours(mask2, c[:1], -1, 255, thickness=cv.FILLED)
#
# kernel2 = np.ones((3, 5), np.uint8)  # Taking a matrix of size 5 as the kernel
# img_erosion2 = cv.erode(mask2, kernel, iterations=1)
# img_dilation2 = cv.dilate(img_erosion2, kernel2, iterations=2)
# img_dilation2 = img_dilation2.astype('uint8')
#
# im3, contours01, _ = cv.findContours(img_dilation2.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)  # hierarchy
# c2 = sorted(contours01, key=cv.contourArea, reverse=True)
# mask3 = np.zeros(shape=(img_dilation2.shape[0], img_dilation2.shape[1]), dtype=np.uint8)
# cv.drawContours(mask3, c2[:1], -1, 255)
#
# mask3 = mask3.astype('float64')
# mask3 = mask3/255.0

# plt.figure()
# plt.subplot(131)
# plt.imshow(np.array(im), cmap=plt.cm.bone)
# plt.title('Original image')
# plt.subplot(132)
# plt.imshow(mask2, cmap=plt.cm.bone)
# plt.title('Largest contour and filled')
# plt.subplot(133)
# plt.imshow(np.array(im), cmap=plt.cm.bone)
# plt.imshow(mask3, cmap=plt.cm.bone, alpha=0.5)
# plt.title('Final contoured image')
# plt.show()

# plt.figure()
# plt.subplot(231)
# plt.imshow(np.array(im), cmap=plt.cm.bone)
# plt.title('Original image')
# plt.subplot(232)
# plt.imshow(obj_changed, cmap=plt.cm.bone)
# plt.title('Contrast enhanced')
# plt.subplot(233)
# plt.imshow(w, cmap=plt.cm.bone)
# plt.title('Thresholded')
# plt.subplot(234)
# plt.imshow(img_dilation, cmap=plt.cm.bone)
# plt.title('Erosion and Dilation')
# # plt.subplot(235)
# # plt.imshow(mask1, cmap=plt.cm.bone)
# # plt.title('Contoured')
# plt.subplot(235)
# plt.imshow(mask2, cmap=plt.cm.bone)
# plt.title('Largest contour and filled')
# # plt.subplot(236)
# # plt.imshow(img_dilation2, cmap=plt.cm.bone)
# # plt.title('Largest contour morph ops')
# plt.subplot(236)
# plt.imshow(np.array(im), cmap=plt.cm.bone)
# plt.imshow(mask3, cmap=plt.cm.bone, alpha=0.5)
# # plt.imshow(mask3, cmap=plt.cm.bone)
# plt.title('Final contoured image')
# plt.show()