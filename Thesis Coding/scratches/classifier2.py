import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data

    w = unique_labels(y_true, y_pred)
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

def read_data(file_path1, file_path2):
    data_f = pd.read_csv(file_path1).to_numpy()
    label_f = pd.read_csv(file_path2).to_numpy()
    return data_f, label_f

X, y = read_data("C:\\Users\\user\\Desktop\\unet-master\\unet-master\\data_segm.csv", "C:\\Users\\user\\Desktop\\unet-master\\unet-master\\class_segm.csv")

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=42)
#####################################
# PLOT ROC curves and AUC
# precision, recall


# _count_miss = []
# _acc = []
#
# for cnt in range(1, 26):

classifier = KNeighborsClassifier(n_neighbors=6)
classifier.fit(X_train, y_train)

y_pred = classifier.predict(X_test)
y_pred = np.hstack(y_pred)
y_test = np.hstack(y_test)

# how did our model perform?
count_misclassified = (y_test != y_pred).sum()
accuracy = metrics.accuracy_score(y_test, y_pred)

print('Misclassified samples: {}'.format(count_misclassified))
print('Out of', y_pred.shape[0])

print('Accuracy: {:.2f}'.format(accuracy))
# print(cnt)

    # _count_miss.append(count_misclassified)
    # _acc.append(accuracy)


# x = np.linspace(1, 25, 25)
# plt.figure()
# plt.subplot(121)
# plt.plot(x, _acc)
# plt.title('Test accuracy using k-NN alg')
# plt.xlabel('# of k-NN')
# plt.ylabel('test accuracy %')
# plt.subplot(122)
# plt.plot(x, _count_miss)
# plt.title('Misclassified points using k-NN alg')
# plt.xlabel('# of k-NN')
# plt.ylabel('# of misclassified points')
# plt.show()


#########################################
print(classification_report(y_test, y_pred))

y_test[np.where(~(y_test==1))[0]] = 0 # class1 test
y_test[np.where((y_test==1))[0]] = 1
y_pred[np.where(~(y_pred==1))[0]] = 0 # prediction array put 1 if desired class, 0 otherwise
y_pred[np.where((y_pred==1))[0]] = 1

# calculate scores
lr_auc = roc_auc_score(y_test, y_pred)
# summarize scores
print('Logistic: ROC AUC=%.3f for breath_holding' % (lr_auc))
# calculate roc curves
lr_fpr, lr_tpr, _ = roc_curve(y_test, y_pred)

# precision, recall
lr_precision, lr_recall, _ = precision_recall_curve(y_test, y_pred)

print('Precision:', lr_precision, 'Recall:', lr_recall)

# plot the roc curve for the model
plt.figure()
plt.plot(lr_fpr, lr_tpr, marker='.', label='KNN', c='C1')
# axis labels
plt.title('ROC curve for cough classification')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
# show the legend
plt.legend()
# show the plot
plt.show()

# class_names=np.asarray(["dummy", "breath-hold", "cough", "deep", "regular", "shallow"])
#
# # Plot normalized confusion matrix
# plot_confusion_matrix(y_test, y_pred, classes=class_names,
#                       title='Confusion matrix')
#
# plt.show()