import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import savgol_filter as savgol
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA

name = ['breath_hold', 'coughing', 'deep', 'regular', 'shallow']
filename = 'C:\\Users\\lappa\\Desktop\\Class_Data'

_arr = []
_mean = []
_median = []
superior_mean = []
inferior_mean = []
_meanARR = []
_medianARR = []
_peaks = []
_valleys = []

for n in range(len(name)):

    a = pd.read_csv(filename + '\\' + name[n] + '.csv', usecols=[0])
    df = a['depth']
    d = df.to_numpy()
    SG = savgol(d, 51, 7)
    x = np.linspace(0, SG.shape[0], SG.shape[0])
    y = np.zeros(shape=(x.shape[0]))

    diff_a = np.diff(np.sign(np.diff(SG))).nonzero()[0] + 1  # local min & max - detect non zero points
    diff_b = (np.diff(np.sign(np.diff(SG))) > 0).nonzero()[0] + 1  # local min - inferior
    diff_c = (np.diff(np.sign(np.diff(SG))) < 0).nonzero()[0] + 1  # local max - superior
    # +1 due to the fact that diff reduces the original index number

    # plt.figure(figsize=(12, 5))
    # plt.plot(SG, color='grey', ls='--', label="surrogate signal")
    # # plt.plot(x, y, color='black')
    # # plt.plot(x[diff_b], SG[diff_b], "o", label="min-trough", color='r')
    # # plt.plot(x[diff_c], SG[diff_c], "o", label="max-peak", color='b')
    #
    # # plt.plot(x[diff_b], SG[diff_total], label="superior-inferior", color='g')
    # plt.title('Magnitude of surrogate signal-' + name[n])
    # plt.xlabel('number of samples')
    # plt.ylabel('Magnitude in mm')
    # plt.legend(loc='best')
    # plt.show()

    if SG[diff_c].shape[0] >= SG[diff_b].shape[0]:
        cut_len = SG[diff_b].shape[0]
    else:
        cut_len = SG[diff_c].shape[0]

    SG_c = SG[diff_c][:cut_len] # peaks
    SG_b = SG[diff_b][:cut_len] # valleys

    arr_mean = np.mean(np.expand_dims((SG_c - SG_b),axis=1), axis=1)
    arr_median = np.median(np.expand_dims((SG_c - SG_b),axis=1), axis=1)

    superior_mean.append(np.mean(SG[diff_c])) # mean of superior point of a breathing pattern
    inferior_mean.append(np.mean(SG[diff_c])) # mean of inferior point of a breathing pattern

    _arr.append(SG)
    _mean.append(np.mean(SG))
    _median.append(np.median(SG)) # median of every breathing pattern
    _meanARR.append(arr_mean)
    _medianARR.append(arr_median)
    _peaks.append(SG_c)
    _valleys.append(SG_b)

PV_bh = _peaks[0] - _valleys[0]
PV_cough = _peaks[1] - _valleys[1]
PV_deep = _peaks[2] - _valleys[2]
PV_regular = _peaks[3] - _valleys[3]
PV_shallow = _peaks[4] - _valleys[4]

class_bh = np.zeros(shape=(PV_bh.shape[0]))
class_bh[:] = 1
class_cough = np.zeros(shape=(PV_cough.shape[0]))
class_cough[:] = 2
class_deep = np.zeros(shape=(PV_deep.shape[0]))
class_deep[:] = 3
class_regular = np.zeros(shape=(PV_regular.shape[0]))
class_regular[:] = 4
class_shallow = np.zeros(shape=(PV_shallow.shape[0]))
class_shallow[:] = 5

a_new = pd.read_csv('C:\\Users\\lappa\\Desktop\\Class_Data\\regular.csv', usecols=[0])
df_new = a_new['depth']
d_new = df_new.to_numpy()

# Replace in y1,y2 .... with SG_new = d_new.shape[0] if you want only points of mean and median

SG_new = d_new.shape[0]
x2 = np.linspace(0, SG_new, SG_new)
y1 = np.zeros(shape=(PV_bh.shape[0]))
y1[:] = _mean[0]
y2 = np.zeros(shape=(PV_cough.shape[0]))
y2[:] = _mean[1]
y3 = np.zeros(shape=(PV_deep.shape[0]))
y3[:] = _mean[2]
y4 = np.zeros(shape=(PV_regular.shape[0]))
y4[:] = _mean[3]
y5 = np.zeros(shape=(PV_shallow.shape[0]))
y5[:] = _mean[4]

y6 = np.zeros(shape=(PV_bh.shape[0]))
y6[:] = _median[0]
y7 = np.zeros(shape=(PV_cough.shape[0]))
y7[:] = _median[1]
y8 = np.zeros(shape=(PV_deep.shape[0]))
y8[:] = _median[2]
y9 = np.zeros(shape=(PV_regular.shape[0]))
y9[:] = _median[3]
y10 = np.zeros(shape=(PV_shallow.shape[0]))
y10[:] = _median[4]

bh = superior_mean[0] - inferior_mean[0] # S-I breath holding
cough = superior_mean[1] - inferior_mean[1] # S-I coughing
deep = superior_mean[2] - inferior_mean[2] # S-I deep
regular = superior_mean[3] - inferior_mean[3] # S-I regular
shallow = superior_mean[4] - inferior_mean[4] # S-I shallow

w1 = np.zeros(shape=(SG_new))
w1[:] = bh
w2 = np.zeros(shape=(SG_new))
w2[:] = cough
w3 = np.zeros(shape=(SG_new))
w3[:] = deep
w4 = np.zeros(shape=(SG_new))
w4[:] = regular
w5 = np.zeros(shape=(SG_new))
w5[:] = shallow

# plt.figure()
# plt.title('Mean and median values of different respiratory patterns')
# plt.plot(x2, y1, label='bh', c='C0')
# plt.plot(x2, y2, label='coughing', c='C1')
# plt.plot(x2, y3, label='deep', c='C2')
# plt.plot(x2, y4, label='regular', c='C3')
# plt.plot(x2, y5, label='shallow', c='C4')
# # plt.plot(x2, y6, label='bh_median', c='C5')
# # plt.plot(x2, y7, label='coughing_median', c='C6')
# # plt.plot(x2, y8, label='deep_median', c='C7')
# # plt.plot(x2, y9, label='regular_median', c='C8')
# # plt.plot(x2, y10, label='shallow_median', c='C9')
# plt.legend(loc='best')
# plt.show()

# plt.figure()
# plt.title('Scatterplot of surrogate using 2 new features')
# plt.scatter(w1, y6, label='bh')
# plt.scatter(w2, y7, label='cough')
# plt.scatter(w3, y8, label='deep')
# plt.scatter(w4, y9, label='regular')
# plt.scatter(w5, y10, label='shallow')
# plt.xlabel('Surrogate mean value (mm)')
# plt.ylabel('valleys-peaks difference')
# plt.legend(loc='best')
# plt.show()


# plt.figure()
# plt.title('Scatterplot of surrogate using 2 new features')


# 3D scatterplot
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(PV_bh, y6, class_bh, label='bh')
# ax.scatter(PV_cough, y7, class_cough, label='cough')
# ax.scatter(PV_deep, y8, class_deep, label='deep')
# ax.scatter(PV_regular, y9, class_regular, label='regular')
# ax.scatter(PV_shallow, y10, class_shallow, label='shallow')
# ax.set_xlabel('Valleys-peaks difference' )
# ax.set_ylabel('Surrogate median value (mm)')
# ax.set_zlabel('Respiration pattern')
# plt.legend(loc='best')
# plt.show()



list_PV = [PV_bh, PV_cough, PV_deep, PV_regular, PV_shallow]
list_median = [y6, y7, y8, y9, y10]
list_class = [class_bh, class_cough, class_deep, class_regular, class_shallow]
principalComponents = []

for n_cnt in range(0, len(list_PV)):
    A = np.expand_dims(list_PV[n_cnt], axis=1)
    B = np.expand_dims(list_median[n_cnt], axis=1)
    C = np.expand_dims(list_class[n_cnt], axis=1)
    X = np.concatenate((A, B), axis=1)

    pca = PCA(n_components=1)
    principalComponents.append(pca.fit_transform(X))

# Scatter plot with new feature works amazing!!
# plt.figure()
# plt.title('Scatterplot of new PCA feature')
# plt.scatter(class_bh, principalComponents[0], label='bh')
# plt.scatter(class_cough, principalComponents[1], label='cough')
# plt.scatter(class_deep, principalComponents[2], label='deep')
# plt.scatter(class_regular, principalComponents[3], label='regular')
# plt.scatter(class_shallow, principalComponents[4], label='shallow')
# plt.xlabel('Class')
# plt.ylabel('Principal Component 1')
# plt.legend(loc='best')
# plt.show()

# for i_n in range(0, len(principalComponents)):
#     principalComponents[i_n] = np.expand_dims(principalComponents[i_n], axis=1)

# Doesn't work due to differences in shape of each Principal Component
F_ARR = np.concatenate((principalComponents[0], principalComponents[1], principalComponents[2], principalComponents[3], principalComponents[4]), axis=0)
C_ARR = np.concatenate((class_bh, class_cough, class_deep, class_regular, class_shallow), axis=0)

# dataset = pd.DataFrame({'data': np.squeeze(F_ARR, axis=1), 'class': C_ARR})
#
# # plt.figure()
# dataset.plot(x='class', y='data', kind='bar', legend=False, grid=True, figsize=(8, 5))
# plt.title("Data per class")
# plt.ylabel('# of Occurrences', fontsize=12)
# plt.xlabel('class', fontsize=12)
# plt.show()

# np.savetxt("C:\\Users\\lappa\\Desktop\\new_feat\\data.csv", F_ARR, delimiter=",")
# np.savetxt("C:\\Users\\lappa\\Desktop\\new_feat\\class_data.csv", C_ARR, delimiter=",")

# np.savetxt("C:\\Users\\lappa\\Desktop\\new_feat\\bh.csv", principalComponents[0], delimiter=",")
# np.savetxt("C:\\Users\\lappa\\Desktop\\new_feat\\cough.csv", principalComponents[1], delimiter=",")
# np.savetxt("C:\\Users\\lappa\\Desktop\\new_feat\\deep.csv", principalComponents[2], delimiter=",")
# np.savetxt("C:\\Users\\lappa\\Desktop\\new_feat\\regular.csv", principalComponents[3], delimiter=",")
# np.savetxt("C:\\Users\\lappa\\Desktop\\new_feat\\shallow.csv", principalComponents[4], delimiter=",")




