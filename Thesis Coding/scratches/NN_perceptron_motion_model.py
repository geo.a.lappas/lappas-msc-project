from random import randrange
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.signal import savgol_filter as savgol
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from sklearn.metrics import accuracy_score
import itertools
from keras.models import Sequential
from keras.layers import Dense
from keras.activations import hard_sigmoid
from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense
from keras import backend as K
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

def root_mean_squared_error(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true)))

def array_equal_size(arr_1, arr_2, cut_m=1):
    '''
    Reduce the size of one sample to make them equal size. The sides of the biggest signal are truncated

    Args:
        arr_1 (1d array/list): signal for example the reference & arr_2 (1d array/list): signal for example the target
        cut_m (int): cutting mode either 1 or 2
    Returns:
        if cut_m == 1: cutting the larger signal at the beginning and end of the using half of the difference at each part
            arr_1 (1d array/list) & arr_2 (1d array/list) - cutted versions
        if cut_m == 2: cutting the larger signal at the end of using the difference between the two signals
            arr_1 (1d array/list) & arr_2 (1d array/list) - cutted versions
        dif_length (int): size diffence between the two original arrays
    '''

    len_1, len_2 = len(arr_1), len(arr_2)
    dif_length = len_1-len_2

    if dif_length<0:
        if cut_m == 1: arr_2 = arr_2[int(np.floor(-dif_length/2)):len_2-int(np.ceil(-dif_length/2))]
        else: arr_2 = arr_2[0:len_2 - int(np.ceil(-dif_length))]
    elif dif_length>0:
        if cut_m == 1: arr_1 = arr_1[int(np.floor(dif_length/2)):len_1-int(np.ceil(dif_length/2))]
        else: arr_1 = arr_1[0:len_1 - int(np.ceil(dif_length))]

    return arr_1, arr_2, dif_length

if __name__ == "__main__":
    a = pd.read_csv('C:\\Users\\user\\Desktop\\10_Nov_NN_motion_model\\InternalMotionSignal\\Resampled E_internal_motion.csv', usecols=[0])
    df_a = a['displacement']
    d_a = df_a.to_numpy()

    b = pd.read_csv('C:\\Users\\user\\Desktop\\10_Nov_NN_motion_model\\SurrogateSignal\\Ecartesian.csv', usecols=[0])
    df_b = b['depth2667']
    d_b = df_b.to_numpy()

    ref_signal = d_b # US signal - pulse receiver magnitude
    shift_signal = d_a # MRI signal - displacement (mm) in SI

    # smoothing process
    SG_ref_7 = savgol(ref_signal, 51, 7)
    SG_shift = savgol(shift_signal, 21, 4)

    # Scaling based on Reference signal - internal motion
    mRef = np.mean(SG_ref_7)
    stdRef = np.std(SG_ref_7)
    mSig = np.mean(SG_shift)
    stdSig = np.std(SG_shift)
    SG_ref_7 = ((SG_ref_7 - mRef) / stdRef) * stdSig + mSig
    SG_shift_scaled = SG_shift

    # create same size for internal motion and surrogate signals - cutting difference at the end of signal
    SG_ref_7_1, SG_shift_scaled_1, dif_length = array_equal_size(SG_ref_7, SG_shift_scaled, 2)

    # normalization of the signals
    SG_ref_7_norm = (SG_ref_7_1 - np.mean(SG_ref_7_1)) / (np.std(SG_ref_7_1))
    SG_shift_scaled_norm = (SG_shift_scaled_1 - np.mean(SG_shift_scaled_1)) / (np.std(SG_shift_scaled_1))

    X = np.expand_dims(SG_ref_7_norm, axis=1)
    Y = np.expand_dims(SG_shift_scaled_norm, axis=1)

    def baseline_model():
        model = Sequential()
        model.add(Dense(1, input_dim=1, activation='relu'))
        model.add(Dense(1))
        model.compile(loss=root_mean_squared_error, optimizer='adam') # root_mean_squared_error
        return model

    estimator = KerasRegressor(build_fn=baseline_model, epochs=100, batch_size=100, verbose=False)
    kfold = KFold(n_splits=10, random_state=1)
    results = cross_val_score(estimator, X, Y, cv=kfold)
    print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))

    estimator.fit(X, Y)
    prediction = estimator.predict(X)
    # accuracy_score(Y, prediction)

    train_error = np.abs(Y - prediction)
    mean_error = np.mean(train_error)
    min_error = np.min(train_error)
    max_error = np.max(train_error)
    std_error = np.std(train_error)

    print('Train error:', train_error, 'mean+/-std:', mean_error, std_error)