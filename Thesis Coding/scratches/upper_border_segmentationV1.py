# Contrast and brightness enhancement operations. Along with some morphological operations.
# This script is intended to enhance the detection of the upper border of liver.


# With this code patient E works, but not A, partially B, C, E - not 100% detect more area than the actual liver


import numpy as np
import cv2 as cv
from PIL import Image, ImageEnhance
import matplotlib.pyplot as plt
import os
# import pygame

search = "C:\\Users\\lappa\\Desktop\\PNG_160\\PNG_E_C\\"

# BLACK = (0, 0, 0)
# size = (700, 500)
# screen = pygame.display.set_mode(size)
#
# # Draw an ellipse, using a rectangle as the outside boundaries
# pygame.draw.ellipse(screen, BLACK, [20,20,250,100], 2)

list = sorted(os.listdir(search), key=len)  # dir is your directory path

st = int(list[0].split('.png')[0].split('I')[1])  # start of for loop
# st = 181
en = int(list[-1].split('.png')[0].split('I')[1])  # end of for loop

cnt = st

for i in range(st, en + 1, 1):

    if i <= 99:
        im = Image.open(search + 'I0' + str(i) + '.png')
    else:
        im = Image.open(search + 'I' + str(i) + '.png')

    obj = ImageEnhance.Contrast(im).enhance(70)  # contrast enhancement
    obj_changed = np.array(obj)  # Convert PIL to NP array
    result = np.where((obj_changed < 250))
    w = obj_changed.copy()
    w[result[0], result[1]] = 0
    w = w.astype('float64')
    w = w/255.0

    kernel = np.ones((2, 2), np.uint8)  # Taking a matrix of size 5 as the kernel
    img_erosion = cv.erode(w, kernel, iterations=4)
    img_dilation = cv.dilate(img_erosion, kernel, iterations=1)
    img_dilation = img_dilation.astype('uint8')

    im2, contours0, _ = cv.findContours(img_dilation.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)  # hierarchy
    c = sorted(contours0, key=cv.contourArea, reverse=True) # c = max(contours0, key = cv.contourArea)
    mask1 = np.zeros(shape=(img_dilation.shape[0], img_dilation.shape[1]), dtype=np.uint8)
    mask2 = np.zeros(shape=(img_dilation.shape[0], img_dilation.shape[1]), dtype=np.uint8)
    cv.drawContours(mask1, contours0, -1, 255)
    cv.drawContours(mask2, c[:1], -1, 255, thickness=cv.FILLED)

    kernel2 = np.ones((3, 5), np.uint8)  # Taking a matrix of size 5 as the kernel
    img_erosion2 = cv.erode(mask2, kernel, iterations=1)
    img_dilation2 = cv.dilate(img_erosion2, kernel2, iterations=2)
    img_dilation2 = img_dilation2.astype('uint8')

    im3, contours01, _ = cv.findContours(img_dilation2.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)  # hierarchy
    c2 = sorted(contours01, key=cv.contourArea, reverse=True)
    mask3 = np.zeros(shape=(img_dilation2.shape[0], img_dilation2.shape[1]), dtype=np.uint8)
    cv.drawContours(mask3, c2[:1], -1, 255)

    mask3 = mask3.astype('float64')
    mask3 = mask3/255.0

    img = np.array(im)
    cv.ellipse(img, (74, 74), (125, 50), 0, 360, 255, 5)

    # plt.figure()
    # plt.subplot(121)
    # plt.imshow(np.array(im), cmap=plt.cm.bone)
    # plt.title('Original image %d' %i)
    # # plt.subplot(132)
    # # plt.imshow(mask2, cmap=plt.cm.bone)
    # # plt.title('Largest contour and filled')
    # plt.subplot(122)
    # plt.imshow(np.array(im), cmap=plt.cm.bone)
    # plt.imshow(mask3, cmap=plt.cm.bone, alpha=0.5)
    # plt.title('Final contoured image')
    # #  plt.show(block=False)
    # # plt.pause(1)
    # # plt.close()
    # plt.show()

    plt.figure()
    plt.subplot(231)
    plt.imshow(np.array(im), cmap=plt.cm.bone)
    plt.title('Original image %d' %i)
    plt.subplot(232)
    plt.imshow(obj_changed, cmap=plt.cm.bone)
    plt.title('Contrast enhanced')
    plt.subplot(233)
    plt.imshow(w, cmap=plt.cm.bone)
    plt.title('Thresholded')
    plt.subplot(234)
    plt.imshow(img_dilation, cmap=plt.cm.bone)
    plt.title('Erosion and Dilation')
    # plt.subplot(235)
    # plt.imshow(mask1, cmap=plt.cm.bone)
    # plt.title('Contoured')
    plt.subplot(235)
    plt.imshow(mask2, cmap=plt.cm.bone)
    plt.title('Largest contour and filled')
    # plt.subplot(236)
    # plt.imshow(img_dilation2, cmap=plt.cm.bone)
    # plt.title('Largest contour morph ops')
    plt.subplot(236)
    plt.imshow(np.array(im), cmap=plt.cm.bone)
    plt.imshow(mask3, cmap=plt.cm.bone, alpha=0.5)
    # plt.imshow(mask3, cmap=plt.cm.bone)
    plt.title('Final contoured image')
    plt.show()