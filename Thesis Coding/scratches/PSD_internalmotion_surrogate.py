import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
from scipy.signal import savgol_filter as savgol
from statsmodels.graphics.tsaplots import plot_acf
import numpy as np

name1 = ['Acartesian', 'Bcartesian', 'Ccartesian', 'Dcartesian', 'Ecartesian']
filename1 = 'C:\\Users\\lappa\\Desktop\\ThesisData\\SurrogateSignal'
name = ['Resampled A_internal_motion', 'Resampled B_internal_motion', 'Resampled C_internal_motion', 'Resampled D_internal_motion', 'Resampled E_internal_motion']
filename = 'C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\'

SG_com = []
_arr = []

for n in range(len(name1)):

    a = pd.read_csv(filename1 + '\\' + name1[n] + '.csv', usecols=[0])
    df = a['depth2667']
    d = df.to_numpy()
    SG = savgol(d, 51, 7)
    _arr.append(SG)

freqs_A1, psd_A1 = signal.welch(_arr[0])
freqs_C1, psd_C1 = signal.welch(_arr[2])
freqs_D1, psd_D1 = signal.welch(_arr[3])
freqs_E1, psd_E1 = signal.welch(_arr[4])

freqs_normA1 = (freqs_A1 - min(freqs_A1)) / (max(freqs_A1) - min(freqs_A1))
psd_normA1 = (psd_A1 - min(psd_A1)) / (max(psd_A1) - min(psd_A1))
freqs_normC1 = (freqs_C1 - min(freqs_C1)) / (max(freqs_C1) - min(freqs_C1))
psd_normC1 = (psd_C1 - min(psd_C1)) / (max(psd_C1) - min(psd_C1))
freqs_normD1 = (freqs_D1 - min(freqs_D1)) / (max(freqs_D1) - min(freqs_D1))
psd_normD1 = (psd_D1 - min(psd_D1)) / (max(psd_D1) - min(psd_D1))
freqs_normE1 = (freqs_E1 - min(freqs_E1)) / (max(freqs_E1) - min(freqs_E1))
psd_normE1 = (psd_E1 - min(psd_E1)) / (max(psd_E1) - min(psd_E1))

for m in range(len(name)):
    a = pd.read_csv(filename + name[m] + '.csv', usecols=[0])
    df = a['displacement']
    d = df.to_numpy()
    SG = d
    SG_com.append(SG)

freqs_A, psd_A = signal.welch(SG_com[0])
freqs_C, psd_C = signal.welch(SG_com[2])
freqs_D, psd_D = signal.welch(SG_com[3])
freqs_E, psd_E = signal.welch(SG_com[4])

freqs_normA = (freqs_A - min(freqs_A)) / (max(freqs_A) - min(freqs_A))
psd_normA = (psd_A - min(psd_A)) / (max(psd_A) - min(psd_A))
freqs_normC = (freqs_C - min(freqs_C)) / (max(freqs_C) - min(freqs_C))
psd_normC = (psd_C - min(psd_C)) / (max(psd_C) - min(psd_C))
freqs_normD = (freqs_D - min(freqs_D)) / (max(freqs_D) - min(freqs_D))
psd_normD = (psd_D - min(psd_D)) / (max(psd_D) - min(psd_D))
freqs_normE = (freqs_E - min(freqs_E)) / (max(freqs_E) - min(freqs_E))
psd_normE = (psd_E - min(psd_E)) / (max(psd_E) - min(psd_E))

# plt.figure()
# # plt.plot(freqs_normA, psd_normA, label='liver A SI-motion')
# # plt.plot(freqs_normA1, psd_normA1, ls='--', label='surrogate magnitude')
# # plt.plot(freqs_normC, psd_normC, label='liver C SI-motion')
# # plt.plot(freqs_normC1, psd_normC1, label='surrogate magnitude')
# # plt.plot(freqs_normD, psd_normD, label='liver D SI-motion')
# # plt.plot(freqs_normD1, psd_normD1, label='surrogate magnitude')
# plt.plot(freqs_normE, psd_normE, label='liver E SI-motion')
# plt.plot(freqs_normE1, psd_normE1, label='surrogate magnitude')
# # plt.xlim([0, 1])
# plt.title('Normalized PSD')
# plt.xlabel('Frequency')
# plt.ylabel('Power')
# plt.legend(loc='best')
# plt.show()

df_A = pd.DataFrame(SG_com[0])

SG_com[0] = np.expand_dims(SG_com[0], axis=1)

plot_acf(df_A, unbiased=True)
plt.show()


