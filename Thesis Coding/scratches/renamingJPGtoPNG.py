# Python 3 code to rename multiple files in a directory or folder

# importing os module
import os


folder_path = "C:\\Users\\lappa\\Desktop\\Twente\\Master Thesis\\Practical\\NN models\\unet-master\\unet-master\\data\\membrane\\train\\image\\"

# Function to rename multiple files
def main():
    i = 0

    for filename in os.listdir(folder_path):

        # n_name = filename.split('I')[1]
        n_name = filename.split('.png')[0]
        dst = n_name + ".png"
        src = filename

        print("filename is: %s" % filename)
        print("The counter is: %d" % i)

        # rename() function will rename all the files
        os.rename(folder_path + src, folder_path + 'image' + dst)
        i += 1

# Driver Code
if __name__ == '__main__':
    # Calling main() function
    main()
