import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.signal import savgol_filter as savgol
from scipy.signal import medfilt as medfilt_sc

# name = ['A_C_internal_motion', 'B_C_internal_motion', 'C_C_internal_motion', 'D_C_internal_motion', 'E_C_internal_motion']
# filename = 'C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\'

name = ['A_internal_motion', 'B_internal_motion', 'C_internal_motion', 'D_internal_motion', 'E_internal_motion']
filename = 'C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\Resampled '

_arr = []
_arr_origin = []
_arr_med = []

superior_mean = []
superior_std = []
superior_var = []
inferior_mean = []
inferior_std = []
inferior_var = []
median_var = []

for n in range(len(name)):
    a = pd.read_csv(filename + name[n] + '.csv', usecols=[0])
    df = a['displacement']
    d = df.to_numpy()
    d = d[0:6000]
    SG = savgol(d, 21, 4)
    median_filt = medfilt_sc(d, 199)

    x = np.linspace(0,SG.shape[0],SG.shape[0])
    y = np.zeros(shape=(x.shape[0]))

    # print(x)
    diff_a = np.diff(np.sign(np.diff(SG))).nonzero()[0] + 1  # local min & max - detect non zero points
    diff_b = (np.diff(np.sign(np.diff(SG))) > 0).nonzero()[0] + 1  # local min - inferior
    diff_c = (np.diff(np.sign(np.diff(SG))) < 0).nonzero()[0] + 1  # local max - superior
    # +1 due to the fact that diff reduces the original index number

    median_x = np.median(diff_c)
    median_var.append(median_x)


    # plot
    plt.figure(figsize=(12, 5))
    plt.plot(x, SG, color='C0', ls='--', label="SG_filtered")
    plt.plot(x, median_filt, color='C1', ls='--', label="median_filtered")
    # plt.plot(x, d, color='C2', ls='--', label="original")
    # plt.plot(x, y, color='black')
    # plt.plot(x[diff_b], SG[diff_b], "o", label="min-inferior", color='r')
    # plt.plot(x[diff_c], SG[diff_c], "o", label="max-superior", color='b')
    plt.title('Upper liver border motion over time displayed in mm')
    plt.xlabel('number of samples')
    plt.ylabel('Displacement of the liver in SI-direction')
    plt.legend(loc='best')
    plt.show()

    _arr.append(SG)
    _arr_origin.append(d)
    _arr_med.append(median_filt)
    # _mean.append(np.mean(SG))
    # _std.append(np.std(SG))

    # print('Stop')

    superior_mean.append(np.mean(SG[diff_c]))
    inferior_mean.append(np.mean(SG[diff_b]))
    superior_std.append(np.std(SG[diff_c]))
    inferior_std.append(np.std(SG[diff_b]))
    superior_var.append(np.var(SG[diff_c]))
    inferior_var.append(np.var(SG[diff_b]))

    fs = 10e3

    # f, Pxx_den = signal.periodogram(SG)
    # plt.semilogy(f, Pxx_den)
    # # plt.ylim([0, 1])
    # # plt.ylim([1e-7, 1e2])
    # plt.xlabel('frequency [Hz]')
    # plt.ylabel('PSD [V**2/Hz]')
    # plt.show()

plt.figure()
plt.title('All subjects surrogate signal - complete experiment')
plt.xlabel('# samples')
plt.ylabel('Magnitude (mm)')
# plt.plot(df.to_numpy(), c='C1')
plt.plot(_arr[0], c='C0', ls ='--', label='Subject A')
plt.plot(_arr[1], c='C1', label='Subject B')
plt.plot(_arr[2], c='C2', label='Subject C')
plt.plot(_arr[3], c='C3', label='Subject D')
plt.plot(_arr[4], c='C4', label='Subject E')
plt.legend()
plt.show()

total_var_A = superior_var[0] + inferior_var[0]
total_std_A = np.sqrt(np.sum(total_var_A))
total_var_B = superior_var[1] + inferior_var[1]
total_std_B = np.sqrt(np.sum(total_var_B))
total_var_C = superior_var[2] + inferior_var[2]
total_std_C = np.sqrt(np.sum(total_var_C))
total_var_D = superior_var[3] + inferior_var[3]
total_std_D = np.sqrt(np.sum(total_var_D))
total_var_E = superior_var[4] + inferior_var[4]
total_std_E = np.sqrt(np.sum(total_var_E))

A = superior_mean[0] - inferior_mean[0]
B = superior_mean[1] - inferior_mean[1]
C = superior_mean[2] - inferior_mean[2]
D = superior_mean[3] - inferior_mean[3]
E = superior_mean[4] - inferior_mean[4]

print('Subject A:', format(A, '.2f'), '\u00B1', format(total_std_A, '.2f'))
print('Subject B:', format(B, '.2f'), '\u00B1', format(total_std_B, '.2f'))
print('Subject C:', format(C, '.2f'), '\u00B1', format(total_std_C, '.2f'))
print('Subject D:', format(D, '.2f'), '\u00B1', format(total_std_D, '.2f'))
print('Subject E:', format(E, '.2f'), '\u00B1', format(total_std_E, '.2f'), "\n")

print("Subject A-superior:", format(superior_mean[0], '.2f'), "\u00B1", format(superior_std[0], '.2f'))
print("Subject B-superior:", format(superior_mean[1], '.2f'), "\u00B1", format(superior_std[1], '.2f'))
print("Subject C-superior:", format(superior_mean[2], '.2f'), "\u00B1", format(superior_std[2], '.2f'))
print("Subject D-superior:", format(superior_mean[3], '.2f'), "\u00B1", format(superior_std[3], '.2f'))
print("Subject E-superior:", format(superior_mean[4], '.2f'), "\u00B1", format(superior_std[4], '.2f'), "\n")
print("Subject A-inferior:", format(inferior_mean[0], '.2f'), "\u00B1", format(inferior_std[0], '.2f'))
print("Subject B-inferior:", format(inferior_mean[1], '.2f'), "\u00B1", format(inferior_std[1], '.2f'))
print("Subject C-inferior:", format(inferior_mean[2], '.2f'), "\u00B1", format(inferior_std[2], '.2f'))
print("Subject D-inferior:", format(inferior_mean[3], '.2f'), "\u00B1", format(inferior_std[3], '.2f'))
print("Subject E-inferior:", format(inferior_mean[4], '.2f'), "\u00B1", format(inferior_std[4], '.2f'))




