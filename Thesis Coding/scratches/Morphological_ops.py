# Morphological operations - This script is intended to enhance the image contour for an improved ROI of liver.
from scipy import ndimage
import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv

folder_path = "C:\\Users\\lappa\\Desktop\\PNG_160\\PNG_A_C\\"

# png image
prev_gray = cv.imread(folder_path + "I108.png", 0) # ,0

# defining the sobel filters
# sobel_horizontal = np.array([np.array([1, 2, 1]), np.array([0, 0, 0]), np.array([-1, -2, -1])])
# print(sobel_horizontal, 'is a kernel for detecting horizontal edges')
#
# sobel_vertical = np.array([np.array([-1, 0, 1]), np.array([-2, 0, 2]), np.array([-1, 0, 1])])
# print(sobel_vertical, 'is a kernel for detecting vertical edges')
#
# out_h = ndimage.convolve(prev_gray, sobel_horizontal, mode='reflect')
# out_v = ndimage.convolve(prev_gray, sobel_vertical, mode='reflect')
#
# plt.imshow(out_h, cmap='gray')
# plt.show()
#
# kernel_laplace = np.array([np.array([1, 1, 1]), np.array([1, -8, 1]), np.array([1, 1, 1])])
# print(kernel_laplace, 'is a laplacian kernel')
#
# out_l = ndimage.convolve(prev_gray, kernel_laplace, mode='reflect')
#
# plt.imshow(out_l, cmap='gray')
plt.show()

# Mask
mask_arr = np.zeros(shape=(len(prev_gray[0]), len(prev_gray[1])), dtype=np.uint8)

# Taking a matrix of size 5 as the kernel
kernel = np.ones((5, 5), np.uint8)

# The third parameter is the number of iterations, which will determine how much you want to erode/dilate a given image.

img_erosion = cv.erode(prev_gray, kernel, iterations=1)
img_dilation = cv.dilate(img_erosion, kernel, iterations=1)

contours0, hierarchy = cv.findContours(img_dilation.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
cv.drawContours(mask_arr, contours0, -1, 255)

# plt.figure()
# plt.subplot(121)
# plt.imshow(prev_gray, cmap=plt.cm.bone)
# plt.subplot(122)
# plt.imshow(mask_arr, cmap=plt.cm.bone)
# plt.show()