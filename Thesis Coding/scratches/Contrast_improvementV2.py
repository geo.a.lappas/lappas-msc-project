# Contrast and brightness enhancement operations. This script is intended to enhance the visible vessels of liver.

import numpy as np
import cv2 as cv
from PIL import Image, ImageEnhance
import matplotlib.pyplot as plt
import os

def annot_fun(folder_path, save_path, plot_var):

    list = sorted(os.listdir(folder_path), key=len) # dir is your directory path

    st = int(list[0].split('.png')[0].split('I')[1]) # start of for loop
    # st = 181
    en = int(list[-1].split('.png')[0].split('I')[1]) # end of for loop

    cnt = st
    flag_c = 0
    flag_cd = []
    contour_list = []

    for i in range(st, en + 1, 1): # NOTE possible raises EOF error for the ImageEnhancement function

        # open PNG image
        if i <= 99: im = Image.open(folder_path + 'I' + str(i) + '.png').convert('L')
        else: im = Image.open(folder_path + 'I' + str(i) + '.png').convert('L')

        # brightness enhancement - 1.0 values give original image
        obj = ImageEnhance.Brightness(im).enhance(0.005) # value chosen by trial & error
        obj_changed = np.array(obj) # Convert PIL to NP array
        img_dilation = obj_changed
        # # Morphological ops - dilation is expansion of area
        # kernel = np.ones((5, 5), np.uint8)  # Taking a matrix of size 5 as the kernel
        # img_dilation = cv.dilate(obj_changed, kernel, iterations=1)
        # # Number of iterations will determine how much you want to dilate a given image

        # A = int((img_dilation.shape[0] / 3) - 40) # left upper and lower corners
        A = 40
        B = int(img_dilation.shape[0]- 40)
        # B = int(img_dilation.shape[0] - A) # right upper and lower corners
        C = len(img_dilation) # size of image

        # Creating of BoundingBox
        for a in range(0, A): img_dilation[a, :] = 0
        for a in range(B, C): img_dilation[a, :] = 0
        for b in range(0, A): img_dilation[:, b] = 0
        for b in range(B, C): img_dilation[:, b] = 0

        # find contours in the original image
        contours0, hierarchy,= cv.findContours(img_dilation.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE) # hierarchy
        contour_list.append(contours0) # keep a list with contours

        # if contour found set flag to False
        if len(contours0) != 0: flag_cd.append(False)
        # if no contour found raise flag
        elif len(contours0) == 0:
            flag_cd.append(True)
            flag_c += 1 # counter for not found contours
            print('Image %d: no contour has been found - %d time.' %(cnt, flag_c))
            # check whether previous frame has contour
            if flag_cd[cnt - (st+1)]:
                # look for the first False or for the last contour found, backwards searching
                cnt_f = cnt - (st+1)
                s = 1
                while cnt_f >= 0:
                    if flag_cd[cnt_f-s] == False:
                        contours1 = contour_list[cnt_f-s]
                        break
                    else: s += 1
            # otherwise take the previous frame
            else: contours1 = contour_list[cnt - (st+1)]
            cv.drawContours(img_dilation, contours1, -1, 255, thickness=cv.FILLED)

        cnt += 1 # increase the counter

        back = cv.cvtColor(np.array(im.copy()),cv.COLOR_GRAY2RGB)
        cv.drawContours(back, contours0, -1, (0, 255, 0)) # , thickness=cv.FILLED
        gray_dil = np.uint8(cv.cvtColor(img_dilation, cv.COLOR_GRAY2RGB))  # extra array used due to error given
        cv.drawContours(gray_dil, contours0, -1, (255, 255, 255), thickness=cv.FILLED)
        new_im = np.uint8(Image.fromarray(gray_dil)) # conversion to uint8 for saving reasons

        if plot_var:
            plt.figure()
            plt.subplot(141)
            plt.imshow(im, cmap=plt.cm.bone)
            plt.title('Original image %d' %cnt)
            plt.subplot(142)
            plt.imshow(back)
            plt.title('Annotated image')
            plt.subplot(143)
            plt.imshow(img_dilation, cmap=plt.cm.bone)
            plt.title('Labelled image')
            plt.subplot(144)
            plt.imshow(new_im)
            plt.title('Saved image')
            plt.show()
            # plt.show(block=False)
            # plt.pause(1)
            # plt.close()

        else:
            cv.imwrite(save_path + 'L' + str(i) + '.png', new_im)
            if np.sum(new_im) == 0: print('Zero-value saved image.') # check if saved image is empty

search = "C:\\Users\\lappa\\Desktop\\PNG_160\\PNG_E_C\\"
save = "C:\\Users\\lappa\\Desktop\\LABELS\\E_Cartesian\\"
plot_v = True

annot_fun(search, save, plot_v)