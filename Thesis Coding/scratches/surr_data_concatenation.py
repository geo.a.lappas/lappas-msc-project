import pandas as pd
import numpy as np

name = ['breath_hold', 'coughing', 'deep', 'regular', 'shallow']
filename = 'C:\\Users\\lappa\\Desktop\\Final_files'

for n in range(len(name)):

    a = pd.read_csv(filename + '\\' + name[n] + '.csv', index_col ="record_num")
    df = a.iloc[:, :].to_numpy()
    df2 = np.concatenate((df[:, 0], df[:, 1], df[:, 2], df[:, 3], df[:, 4], df[:, 5], df[:, 6], df[:, 7], df[:, 8], df[:, 9], df[:, 10], df[:, 11], df[:, 12], df[:, 13]), axis=0)

    (pd.DataFrame(df2)).to_csv(name[n] + '2.csv', index=False)

