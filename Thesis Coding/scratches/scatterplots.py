import numpy as np
import pandas as pd
from scipy.signal import savgol_filter as savgol
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

# Modified code taken from Pearson's work: https://github.com/pearsonkyle/Signal-Alignment
# Corresponding citation:
# @article{Pearson2019,
#   author={Kyle A. Pearson and Caitlin A. Griffith and Robert T. Zellem and Tommi T. Koskinen and Gael M. Roudier},
#   title={Ground-based Spectroscopy of the Exoplanet XO-2b Using a Systematic Wavelength Calibration},
#   journal={The Astronomical Journal},
#   volume={157},
#   number={1},
#   pages={21},
#   url={http://stacks.iop.org/1538-3881/157/i=1/a=21},
#   year={2019}
# }

def equalize_array_size(array1,array2,cut_m=1):
    '''
    reduce the size of one sample to make them equal size.
    The sides of the biggest signal are truncated

    Args:
        array1 (1d array/list): signal for example the reference
        array2 (1d array/list): signal for example the target

    Returns:
        array1 (1d array/list): middle of the signal if truncated
        array2 (1d array/list): middle of the initial signal if there is a size difference between the array 1 and 2
        dif_length (int): size diffence between the two original arrays
    '''

    len1, len2 = len(array1), len(array2)
    dif_length = len1-len2
    if dif_length<0:
        if cut_m == 1: array2 = array2[int(np.floor(-dif_length/2)):len2-int(np.ceil(-dif_length/2))]
        else: array2 = array2[0:len2 - int(np.ceil(-dif_length))]
    elif dif_length>0:
        if cut_m == 1: array1 = array1[int(np.floor(dif_length/2)):len1-int(np.ceil(dif_length/2))]
        else: array1 = array1[0:len1 - int(np.ceil(dif_length))]

    return array1, array2, dif_length

if __name__ == "__main__":

    name = ['Resampled A_internal_motion', 'Resampled B_internal_motion' ,'Resampled C_internal_motion',
            'Resampled D_internal_motion', 'Resampled E_internal_motion']
    name_sur = ['Acartesian', 'Bcartesian', 'Ccartesian', 'Dcartesian', 'Ecartesian']
    filename = 'C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\'
    filename_sur = 'C:\\Users\\lappa\\Desktop\\ThesisData\\SurrogateSignal\\'

    SG_sur = []
    SG_im = []
    SG_PC = []
    SG_corr = []
    SG_class = []
    SG_final = []

    for n in range(len(name)):

        a = pd.read_csv(filename + name[n] + '.csv', usecols=[0])
        df_a = a['displacement']
        df_a = df_a[0:6000]
        d_a = df_a.to_numpy()

        b = pd.read_csv(filename_sur + name_sur[n] + '.csv', usecols=[0])
        df_b = b['depth2667']
        df_b = df_b[0:6000]
        d_b = df_b.to_numpy()

        # class array
        d_c = np.zeros(shape=(d_a.shape[0], ))
        d_c[:] = n

        ref_signal = d_b # US signal - pulse receiver magnitude
        shift_signal = d_a # MRI signal - displacement (mm) in SI

        # smoothing process
        SG_ref_7 = savgol(ref_signal, 51, 7)
        SG_shift = shift_signal

        # Scaling based on Reference signal - internal motion
        mRef = np.mean(SG_ref_7)
        stdRef = np.std(SG_ref_7)
        mSig = np.mean(SG_shift)
        stdSig = np.std(SG_shift)
        SG_ref_7 = ((SG_ref_7 - mRef) / stdRef) * stdSig + mSig
        SG_shift_scaled = SG_shift

        # create same size for internal motion and surrogate signals - cutting difference at the end of signal
        SG_ref_7_1, SG_shift_scaled_1, dif_length = equalize_array_size(SG_ref_7, SG_shift_scaled, 2)

        # normalization of the signals
        SG_ref_7_norm = (SG_ref_7_1 - np.mean(SG_ref_7_1)) / (np.std(SG_ref_7_1))
        SG_shift_scaled_norm = (SG_shift_scaled_1 - np.mean(SG_shift_scaled_1)) / (np.std(SG_shift_scaled_1))

        # # Standardizing the features
        # SG_ref_7_norm = StandardScaler().fit_transform(SG_ref_7_1)
        # SG_shift_scaled_norm = StandardScaler().fit_transform(SG_shift_scaled_1)

        SG_ref_7_norm = np.expand_dims(SG_ref_7_norm, axis=1)
        SG_shift_scaled_norm = np.expand_dims(SG_shift_scaled_norm, axis=1)
        d_c = np.expand_dims(d_c, axis=1)

        # for every subject stored here
        SG_sur.append(SG_ref_7_norm)
        SG_im.append(SG_shift_scaled_norm)
        SG_class.append(d_c)

        X = np.concatenate((SG_ref_7_norm, SG_shift_scaled_norm), axis=1)

        pca = PCA(n_components=2)
        principalComponents = pca.fit_transform(X)
        SG_PC.append(principalComponents)

        w, v = np.linalg.eig(SG_ref_7_norm * SG_shift_scaled_norm.T)

        print("Eigenvalues:", w)
        print("Eigenvector:", v)

        # print("Covariance of:", n+1, ":", pca.get_covariance)
        print("Variance per principal component:", pca.explained_variance_ratio_)
        print("Principal axes of:", n+1, ":\n", pca.components_)

        finalDf = np.concatenate((principalComponents, d_c), axis=1)
        SG_final.append(finalDf)

        # print('Stop')
        # SG_corr.append(np.corrcoef(SG_ref_7_norm, SG_shift_scaled_norm))

        # plt.figure()
        # plt.title('Scatterplot of surrogate and internal motion data')
        # plt.scatter(SG_ref_7_norm, SG_shift_scaled_norm) # label='surrogate')
        # # plt.scatter(principalComponents[0], principalComponents[1])
        # plt.xlabel('Surrogate - pulse magnitude (mm)')
        # plt.ylabel('Internal SI-motion displacement (mm)')
        # # plt.legend(loc='best')
        # plt.show()

        # plt.figure() # plot normalized data
        # plt.title('Surrogate and internal motion normalized data representation')  # have same length
        # plt.plot(SG_ref_7_norm, label='Normalized surrogate data', c='C0')  # SG_ref_7_norm
        # plt.plot(SG_shift_scaled_norm, label='Normalized resampled internal data', c='C1') # SG_shift_scaled_norm
        # plt.legend(loc='best')
        # plt.show()

    # plt.figure()
    # plt.title('Correlation of surrogate and internal motion data')
    # plt.scatter(SG_final[0], SG_final[0], label='subjectA')
    # plt.scatter(SG_final[1], SG_final[1],  label='subjectB')
    # plt.scatter(SG_final[2], SG_final[2], label='subjectC')
    # plt.scatter(SG_final[3], SG_final[3], label='subjectD')
    # plt.scatter(SG_final[4], SG_final[4], label='subjectE')
    # plt.legend(loc='best')
    # plt.show()
    # plt.scatter(SG_sur[0], SG_im[0])
    # plt.scatter(SG_sur[1], SG_im[1])
    # plt.scatter(SG_sur[2], SG_im[2])
    # plt.scatter(SG_sur[3], SG_im[3])
    # plt.scatter(SG_sur[4], SG_im[4])

    # plt.scatter(SG_PC[0][0], SG_PC[0][1]) # subject A
    # plt.scatter(SG_PC[1][0], SG_PC[1][1]) # subject E
    # plt.scatter(SG_PC[2][0], SG_PC[2][1]) # subject C
    # plt.scatter(SG_PC[3][0], SG_PC[3][1]) # subject D
    # # plt.scatter(SG_PC[4][0], SG_PC[4][1]) # subject E
    # plt.xlabel('Surrogate - pulse magnitude (mm)')
    # plt.ylabel('Internal SI-motion displacement (mm)')
    # plt.legend(loc='best')
    # plt.show()

