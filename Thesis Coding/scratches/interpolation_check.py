import matplotlib.pyplot as plt
from scipy import interpolate
import numpy as np

x = np.arange(0, 10)
y = np.exp(-x/3.0)
f = interpolate.interp1d(x, y)

xnew = np.arange(0, 9, 0.01)
ynew = f(xnew)   # use interpolation function returned by `interp1d`

plt.figure()
# plt.plot(x, y, 'go', xnew, ynew, 'r-')
plt.plot(x, y, 'go', x, f(x) , 'r-')
# plt.legend('real data', 'interpolated data')
# plt.grid()
plt.show()