import numpy as np
import pandas as pd
from scipy.signal import savgol_filter as savgol
from scipy.signal import butter, correlate
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from sklearn.preprocessing import PolynomialFeatures

def equalize_array_size(array1,array2,cut_m=1):
    '''
    reduce the size of one sample to make them equal size.
    The sides of the biggest signal are truncated

    Args:
        array1 (1d array/list): signal for example the reference
        array2 (1d array/list): signal for example the target

    Returns:
        array1 (1d array/list): middle of the signal if truncated
        array2 (1d array/list): middle of the initial signal if there is a size difference between the array 1 and 2
        dif_length (int): size diffence between the two original arrays
    '''

    len1, len2 = len(array1), len(array2)
    dif_length = len1-len2
    if dif_length<0:
        if cut_m == 1: array2 = array2[int(np.floor(-dif_length/2)):len2-int(np.ceil(-dif_length/2))]
        else: array2 = array2[0:len2 - int(np.ceil(-dif_length))]
    elif dif_length>0:
        if cut_m == 1: array1 = array1[int(np.floor(dif_length/2)):len1-int(np.ceil(dif_length/2))]
        else: array1 = array1[0:len1 - int(np.ceil(dif_length))]

    return array1, array2, dif_length

def rmse(actual: np.ndarray, predicted: np.ndarray):
    """ Root Mean Squared Error """
    return np.sqrt(metrics.mean_squared_error(actual, predicted))

def nrmse(actual: np.ndarray, predicted: np.ndarray):
    """ Normalized Root Mean Squared Error """
    return rmse(actual, predicted) / (actual.max() - actual.min())

if __name__ == "__main__":
    from scipy import signal
    import matplotlib.pyplot as plt

    a = pd.read_csv('C:\\Users\\lappa\\Desktop\\ThesisData\\InternalMotionSignal\\Resampled A_internal_motion.csv', usecols=[0])
    df_a = a['displacement']
    d_a = df_a.to_numpy()

    b = pd.read_csv('C:\\Users\\lappa\\Desktop\\ThesisData\\SurrogateSignal\\Acartesian.csv', usecols=[0])
    df_b = b['depth2667']
    d_b = df_b.to_numpy()

    ref_signal = d_b # US signal - pulse receiver magnitude
    shift_signal = d_a # MRI signal - displacement (mm) in SI

    # smoothing process
    SG_ref_7 = savgol(ref_signal, 51, 7)
    SG_shift = savgol(shift_signal, 21, 4)

    # Scaling based on Reference signal - internal motion
    mRef = np.mean(SG_ref_7)
    stdRef = np.std(SG_ref_7)
    mSig = np.mean(SG_shift)
    stdSig = np.std(SG_shift)
    SG_ref_7 = ((SG_ref_7 - mRef) / stdRef) * stdSig + mSig
    SG_shift_scaled = SG_shift

    # create same size for internal motion and surrogate signals - cutting difference at the end of signal
    SG_ref_7_1, SG_shift_scaled_1, dif_length = equalize_array_size(SG_ref_7, SG_shift_scaled, 2)

    # Polynomial fit - Quadratic

    rmse_train = []
    rmse_test = []
    nrmse_train = []
    nrmse_test = []

    def create_polynomial_regression_model(degree):
        "Creates a polynomial regression model for the given degree"

        X_train, X_test, y_train, y_test = train_test_split(SG_shift_scaled_1, SG_ref_7_1, test_size=0.2,
                                                            random_state=0)

        X_train = np.expand_dims(X_train, axis=1)
        X_test = np.expand_dims(X_test, axis=1)
        y_train = np.expand_dims(y_train, axis=1)
        y_test = np.expand_dims(y_test, axis=1)

        poly_features = PolynomialFeatures(degree=degree)

        # transforms the existing features to higher degree features.
        X_train_poly = poly_features.fit_transform(X_train)

        # fit the transformed features to Linear Regression
        poly_model = LinearRegression()
        poly_model.fit(X_train_poly, y_train)

        # predicting on training data-set
        y_train_predicted = poly_model.predict(X_train_poly)

        # predicting on test data-set
        y_test_predict = poly_model.predict(poly_features.fit_transform(X_test))

        # evaluating the model on training dataset
        rmse_train.append(np.sqrt(metrics.mean_squared_error(y_train, y_train_predicted)))
        nrmse_train.append(nrmse(y_train, y_train_predicted))
        # r2_train = metrics.r2_score(y_train, y_train_predicted)

        # evaluating the model on test dataset
        rmse_test.append(np.sqrt(metrics.mean_squared_error(y_test, y_test_predict)))
        nrmse_test.append(nrmse(y_test, y_test_predict))
        # r2_test = metrics.r2_score(y_test, y_test_predict)

        # print("The model performance for the training set")
        # print("-------------------------------------------")
        # print("RMSE of training set is {}".format(rmse_train))
        # print("R2 score of training set is {}".format(r2_train))
        #
        # print("\n")
        #
        # print("The model performance for the test set")
        # print("-------------------------------------------")
        # print("RMSE of test set is {}".format(rmse_test))
        # print("R2 score of test set is {}".format(r2_test))

    for i_cnt in range(1, 21):
        create_polynomial_regression_model(i_cnt) # 2nd order polynomial

    rmse_train =  np.asarray(rmse_train)
    rmse_test =  np.asarray(rmse_test)

    x = np.zeros(shape=(20, 1), dtype=np.int8)
    for i_cnt1 in range(0, x.shape[0]):
        x[i_cnt1] = i_cnt1 + 1
    # linspace(1, 20, 1)

    plt.figure()
    # plt.subplot(121)
    plt.title('Polynomial fitting degree choice')
    plt.xlabel('order')
    plt.xlim([-1, 21])
    plt.ylabel('RMSE')
    plt.plot(x, rmse_train, label='train error', c='C0')
    plt.plot(x, rmse_train, 'bo')
    plt.plot(x, rmse_test, ls='--', label='test error', c='C1')
    plt.plot(x, rmse_test, 'ro')
    plt.legend(loc='best')
    # plt.subplot(122)
    # plt.title('Polynomial fitting degree choice - NRMSE')
    # plt.xlabel('order')
    # plt.ylabel('NRMSE')
    # plt.plot(nrmse_train, label='train error', c='C0')
    # plt.plot(nrmse_test, ls='--', label='test error', c='C1')
    # plt.legend(loc='best')
    plt.show()
