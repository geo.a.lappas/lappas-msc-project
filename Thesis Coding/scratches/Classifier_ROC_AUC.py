import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn import metrics
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
from sklearn.multiclass import OneVsRestClassifier


def read_data(file_path1, file_path2):
    data_f = pd.read_csv(file_path1, usecols=[0])# , usecols=[0]
    data_f = data_f['new'].to_numpy()
    label_f = pd.read_csv(file_path2, usecols=[0])
    label_f = label_f['class'].to_numpy()
    return data_f, label_f

X, y = read_data("C:\\Users\\user\\Desktop\\unet-master\\unet-master\\data.csv", "C:\\Users\\user\\Desktop\\unet-master\\unet-master\\class_data.csv")
X = np.expand_dims(X, axis=1)
# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=42)
#####################################
# 2nd try
# # Using pipeline - for applying logistic regression and one vs rest classifier
# LogReg_pipeline = Pipeline([
#     ('clf', OneVsRestClassifier(LogisticRegression(solver='sag'), n_jobs=-1)),
# ])
# for category in categories:
#     print('**Processing {} comments...**'.format(category))
#
#     # Training logistic regression model on train data
#     LogReg_pipeline.fit(X_train, y_train[category])
#
#     # calculating test accuracy
#     prediction = LogReg_pipeline.predict(y_test)
#     print('Test accuracy is {}'.format(accuracy_score(y_test[category], prediction)))
#     print("\n")
#############################

# FIRST TRY - 0.3
# clf = LogisticRegression(solver='lbfgs',multi_class='multinomial')
# clf.fit(X_train, y_train)
# y_pred =  clf.predict(X_test)
#
# # how did our model perform?
# count_misclassified = (y_test != y_pred).sum()
# print('Misclassified samples: {}'.format(count_misclassified))
# accuracy = metrics.accuracy_score(y_test, y_pred)
# print('Accuracy: {:.2f}'.format(accuracy))
#############################################



# SECOND TRY - 0.76-0.78 - best using 96 NN 556 misclassifier
# from sklearn.neighbors import KNeighborsClassifier
#
# _count_miss = []
# _acc = []
#
# for cnt in range(1, 101):
#     neigh = KNeighborsClassifier(n_neighbors=cnt)
#     neigh.fit(X_train, y_train)
#
#     y_pred =  neigh.predict(X_test)
#
#     # how did our model perform?
#     count_misclassified = (y_test != y_pred).sum()
#     print('Misclassified samples: {}'.format(count_misclassified))
#     accuracy = metrics.accuracy_score(y_test, y_pred)
#     print('Accuracy: {:.2f}'.format(accuracy))
#
#     print(cnt)
#
#     _count_miss.append(count_misclassified)
#     _acc.append(accuracy)
#
# x = np.linspace(1, 100, 100)
# plt.figure()
# plt.subplot(121)
# plt.plot(x, _acc)
# plt.title('Test accuracy using k-NN alg')
# plt.xlabel('# of k-NN')
# plt.ylabel('test accuracy %')
# plt.subplot(122)
# plt.plot(x, _count_miss)
# plt.title('Misclassified points using k-NN alg')
# plt.xlabel('# of k-NN')
# plt.ylabel('# of misclassified points')
# plt.show()

#######################
# 3RD try - 77% using max depth 8
#
# _count_miss = []
# _acc = []
#
# for cnt in range(1, 101):
#     rfc = RandomForestClassifier(n_estimators=100, max_depth=cnt, random_state=0)
#     rfc.fit(X_train, y_train)
#
#     y_pred =  rfc.predict(X_test)
#
#     # how did our model perform?
#     count_misclassified = (y_test != y_pred).sum()
#     print('Misclassified samples: {}'.format(count_misclassified))
#     accuracy = metrics.accuracy_score(y_test, y_pred)
#     print('Accuracy: {:.2f}'.format(accuracy))
#
#     print(cnt)
#
#     _count_miss.append(count_misclassified)
#     _acc.append(accuracy)
#
# x = np.linspace(1, 100, 100)
# plt.figure()
# plt.subplot(121)
# plt.plot(x, _acc)
# plt.title('Test accuracy using Random Forest Classifier alg')
# plt.xlabel('tree depth')
# plt.ylabel('test accuracy %')
# plt.subplot(122)
# plt.plot(x, _count_miss)
# plt.title('Misclassified points using Random Forest Classifier alg')
# plt.xlabel('tree depth k-NN')
# plt.ylabel('# of misclassified points')
# plt.show()
#########################################
# 4RD try - Ridge Classifier - didn't work
#######################################
# 5TH try - MLP 76% - 3 hidden layers 100 neurons each 594 misclassified
# from sklearn.neural_network import MLPClassifier
#
#
# _count_miss = []
# _acc = []
#
# list_layers = [(100, ), (100, 100, ), (100, 100, 100, ), (100, 100, 100, 100, ), (100, 100, 100, 100, 100, )]
#
# for cnt in range(0, len(list_layers)):
#     mlpc = MLPClassifier(hidden_layer_sizes=list_layers[cnt], activation='relu', max_iter=20000)
#     mlpc.fit(X_train, y_train)
#
#     y_pred = mlpc.predict(X_test)
#
#     # how did our model perform?
#     count_misclassified = (y_test != y_pred).sum()
#     print('Misclassified samples: {}'.format(count_misclassified))
#     accuracy = metrics.accuracy_score(y_test, y_pred)
#     print('Accuracy: {:.2f}'.format(accuracy))
#
#     print(cnt)
#
#     _count_miss.append(count_misclassified)
#     _acc.append(accuracy)
#
# x = np.linspace(1, len(list_layers), len(list_layers))
# plt.figure()
# plt.subplot(121)
# plt.plot(x, _acc)
# plt.title('Test accuracy using MLP alg')
# plt.xlabel('# hidden layers')
# plt.ylabel('test accuracy %')
# plt.subplot(122)
# plt.plot(x, _count_miss)
# plt.title('Misclassified points using MLP alg')
# plt.xlabel('# hidden layers')
# plt.ylabel('# of misclassified points')
# plt.show()

########################
# PLOT ROC curves and AUC
# precision, recall

from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_recall_curve


# _count_miss = []
# _acc = []
# _model = []
#
# cnt_all = 0
# for cnt1 in range(1, 51, 10):
#     for cnt2 in range(1, 1001, 20):
#         for cnt3 in range(1, 101, 20):
#             cnt_all = cnt_all +1
#             print(cnt_all)
#
#             rfc = RandomForestClassifier(min_samples_leaf=cnt3, n_estimators=cnt2, max_depth=cnt1)
#             # rfc.fit(X_train, y_train)
#
#             # neigh = KNeighborsClassifier(n_neighbors=8)
#             rfc.fit(X_train, y_train)
#             y_pred =  rfc.predict(X_test)
#
#             # how did our model perform?
#             count_misclassified = (y_test != y_pred).sum()
#             # print('Misclassified samples: {}'.format(count_misclassified))
#             accuracy = metrics.accuracy_score(y_test, y_pred)
#             # print('Accuracy: {:.2f}'.format(accuracy))
#             _model.append(cnt_all)
#             # print(cnt1)
#
#             _count_miss.append(count_misclassified)
#             _acc.append(accuracy)
#
# pos = _acc.index(max(_acc))
# pos_mis = _count_miss.index(min(_count_miss))
# pos_model = _model[pos]
# pos_model_mis = _model[pos_mis]
#
# print(max(_acc))
# print('Using model:', pos_model)
# print(min(_count_miss))
# print('Using model:', pos_model_mis)
################################


from sklearn.neighbors import KNeighborsClassifier


neigh = KNeighborsClassifier(n_neighbors=96)
neigh.fit(X_train, y_train)

y_pred =  neigh.predict(X_test)

# how did our model perform?
count_misclassified = (y_test != y_pred).sum()
print('Misclassified samples: {}'.format(count_misclassified))
accuracy = metrics.accuracy_score(y_test, y_pred)
print('Accuracy: {:.2f}'.format(accuracy))


y_test[np.where(~(y_test==5))[0]] = 0 # class1 test
y_test[np.where((y_test==5))[0]] = 1
y_pred[np.where(~(y_pred==5))[0]] = 0 # prediction array put 1 if desired class, 0 otherwise
y_pred[np.where((y_pred==5))[0]] = 1

# keep probabilities for the positive outcome only
# y_pred = y_pred[:, 1]

# calculate scores
lr_auc = roc_auc_score(y_test, y_pred)
# summarize scores
print('Logistic: ROC AUC=%.3f for breath_holding' % (lr_auc))
# calculate roc curves
lr_fpr, lr_tpr, _ = roc_curve(y_test, y_pred)

# keep probabilities for the positive outcome only
# lr_probs = lr_probs[:, 1]
# predict class values
# yhat = neigh.predict(X_test)

# precision, recall
lr_precision, lr_recall, _ = precision_recall_curve(y_test, y_pred)

print('Precision:', lr_precision, 'Recall:', lr_recall)

# plot the roc curve for the model
plt.figure()
plt.plot(lr_fpr, lr_tpr, marker='.', label='KNN', c='C1')
# axis labels
plt.title('ROC curve for shallow classification')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
# show the legend
plt.legend()
# show the plot
plt.show()