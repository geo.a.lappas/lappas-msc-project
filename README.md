# Estimation of liver respiratory motion using a surrogate signal based on a Deep Learning approach

This repository contain Python code used to estimate and predict the liver motion due to respiration
using a surrogate signal and Deep learning models. 

The MSc thesis and final presentation are also included in this repository as a reference.

# Setup
1. clone/download this repository
2. install python build packages

# Run
1. Changes to the directories and variables names may need to change.
2. Run each section separately.
3. Python 3.XX is used. 